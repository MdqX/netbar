#!/usr/bin/env python3
import os
import sys

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from main import load_config, load_text, CONFIG_FILE
from mdqxmod import FSDict, auto_detect_browser

config = load_config(CONFIG_FILE, return_config=True)
text = load_text(config, return_text=True)


LANG_FILENAME = 0
LANG_LABEL = 1
TEXT_DIR = 'text'
ACTION_RELOAD = 0
ACTION_RESTART = 1

class MainWindow(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()

        self.resize(500, 500)
        self.setWindowTitle(text.sett_title)

        if config.browser is None and __name__ == '__main__':
            config.browser = auto_detect_browser()
            all_browsers = auto_detect_browser(get_all_browsers=True)
            if len(all_browsers) > 1:
                config.browsers = all_browsers

        self.tabWidget = TabWidget(self)
        layout = QtWidgets.QVBoxLayout()
        menuBar = QtWidgets.QMenuBar()
        closeAction = QtWidgets.QAction(text.sett_close, self)
        closeAction.setShortcut('Ctrl+W')
        if __name__ == '__main__':
            closeAction.triggered.connect(sys.exit)
        else:
            closeAction.triggered.connect(self.close)
        menuBar.addAction(closeAction)
        layout.addWidget(menuBar)

        layout.addWidget(self.tabWidget)
        self.tabWidget.resize(500, 500)

        applyBtn = QtWidgets.QPushButton(text.sett_apply)
        applyBtn.setMaximumWidth(100)
        applyBtn.clicked.connect(self.applySettings)
        applyBtnHBox = QtWidgets.QHBoxLayout()
        applyBtnHBox.addWidget(applyBtn)
        applyBtnHBox.setAlignment(Qt.AlignRight)
        layout.addLayout(applyBtnHBox)

        self.setLayout(layout)
    
    def applySettings(self):
        config.text_editor = self.tabWidget.text_editor_LineEdit.text()
        config.file_mgr = self.tabWidget.file_manager_LineEdit.text()
        config.text = self.tabWidget.languages[self.tabWidget.language_comboBox.currentIndex()][LANG_FILENAME]
        config.use_loading_anim = self.tabWidget.useLoadingAnimCheckBox.isChecked()
        config.use_STOS = self.tabWidget.STOS_CheckBox.isChecked()
        config.browser = self.tabWidget.browser.text()

        print(self.tabWidget.search_engines_boxes[0][0].text(), self.tabWidget.search_engines_boxes[0][1].text())
        se_keymap = {}
        for box in self.tabWidget.search_engines_boxes:
            se_keymap[box[0].text()] = int(box[1].text())
        config.se_keymap = se_keymap

        QtWidgets.QMessageBox.information(self, text.info, text.sett_settings_applied)
        if __name__ == '__main__':
            sys.exit(0)


class TabWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        layout = QtWidgets.QVBoxLayout()
        self.tabs = QtWidgets.QTabWidget()
        self.programmes_tab = QtWidgets.QWidget()
        self.interface_tab = QtWidgets.QWidget()
        self.search_engines_tab = QtWidgets.QWidget()
        self.support_tab = QtWidgets.QWidget()
        self.tabs.resize(500, 500)

        self.tabs.addTab(self.programmes_tab, text.sett_programmes)
        self.tabs.addTab(self.interface_tab, text.sett_interface)
        self.tabs.addTab(self.search_engines_tab, text.sett_search_engines)
        self.tabs.addTab(self.support_tab, text.sett_support)

        # *** Programmes tab
        programmes_tab_layout = QtWidgets.QVBoxLayout(self)

        # -- Browser
        browser_Label = QtWidgets.QLabel(f"{text.sett_browser}:")
        self.browser = QtWidgets.QLineEdit(config.browser)
        browser_HBox = QtWidgets.QHBoxLayout()
        browser_HBox.addWidget(browser_Label)
        browser_HBox.addWidget(self.browser)
        browser_HBox.setAlignment(Qt.AlignLeft)
        programmes_tab_layout.addLayout(browser_HBox)

        # -- Text editor
        text_editor_Label = QtWidgets.QLabel(f"{text.sett_text_editor}:")
        self.text_editor_LineEdit = QtWidgets.QLineEdit(config.text_editor)
        text_editor_selectBtn = QtWidgets.QPushButton(text.sett_select)
        text_editor_selectBtn.clicked.connect(self.selectTextEditor)
        text_editor_HBox = QtWidgets.QHBoxLayout()
        text_editor_HBox.addWidget(text_editor_Label)
        text_editor_HBox.addWidget(self.text_editor_LineEdit)
        text_editor_HBox.addWidget(text_editor_selectBtn)
        text_editor_HBox.setAlignment(Qt.AlignLeft)
        programmes_tab_layout.addLayout(text_editor_HBox)

        # -- File manager
        file_manager_Label = QtWidgets.QLabel(f"{text.sett_file_manager}:")
        self.file_manager_LineEdit = QtWidgets.QLineEdit(config.file_mgr)
        file_manager_selectBtn = QtWidgets.QPushButton(text.sett_select)
        file_manager_selectBtn.clicked.connect(self.selectFileManager)
        file_manager_HBox = QtWidgets.QHBoxLayout()
        file_manager_HBox.addWidget(file_manager_Label)
        file_manager_HBox.addWidget(self.file_manager_LineEdit)
        file_manager_HBox.addWidget(file_manager_selectBtn)
        file_manager_HBox.setAlignment(Qt.AlignLeft)
        programmes_tab_layout.addLayout(file_manager_HBox)

        self.programmes_tab.setLayout(programmes_tab_layout)

        # *** Interface
        interface_tab_layout = QtWidgets.QVBoxLayout(self)

        # -- Language
        language_Label = QtWidgets.QLabel(f"{text.sett_language}:")
        self.language_comboBox = QtWidgets.QComboBox()
        self.languages = []  # structure: list(['en', 'English'], ['pl', 'Polish'], ...)
        for file in os.listdir('text'):
            self.languages.append([file])
        for index, lang in enumerate(self.languages[:]):
            cfg = FSDict(os.path.join(TEXT_DIR, lang[LANG_FILENAME]))
            self.languages[index].append(cfg._LABEL)
        for lang in self.languages:
            self.language_comboBox.addItem(lang[LANG_LABEL])
        current_lang = None
        for lang in self.languages:
            if config.text in lang[LANG_FILENAME]:
                current_lang = lang[LANG_LABEL]
        self.language_comboBox.setCurrentText(current_lang)
        self.language_comboBox.currentIndexChanged.connect(self.langChanged)
        language_HBox = QtWidgets.QHBoxLayout()
        language_HBox.addWidget(language_Label)
        language_HBox.addWidget(self.language_comboBox)
        language_HBox.setAlignment(Qt.AlignLeft)
        interface_tab_layout.addLayout(language_HBox)

        # -- Loading anim
        self.useLoadingAnimCheckBox = QtWidgets.QCheckBox(text.sett_use_loading_animation)
        self.useLoadingAnimCheckBox.setChecked(config.use_loading_anim)
        interface_tab_layout.addWidget(self.useLoadingAnimCheckBox)

        # -- STOS
        self.STOS_CheckBox = QtWidgets.QCheckBox(text.sett_use_STOS)
        self.STOS_CheckBox.setChecked(config.use_STOS)
        interface_tab_layout.addWidget(self.STOS_CheckBox)

        self.interface_tab.setLayout(interface_tab_layout)

        # *** Search engines tab
        self.search_engines_grid = QtWidgets.QGridLayout()
        self.search_engines_boxes = []
        row = 0
        for se, index in config.se_keymap.dict.items():
            name = QtWidgets.QLineEdit(se)
            index = QtWidgets.QLineEdit(str(index))
            self.search_engines_boxes.append([name, index])
            self.search_engines_grid.addWidget(name, row, 0)
            self.search_engines_grid.addWidget(index, row, 1)
            row += 1
        self.search_engines_tab.setLayout(self.search_engines_grid)

        layout.addWidget(self.tabs)
        self.setLayout(layout)

        self.required_action = ACTION_RELOAD

    def selectTextEditor(self):
        text_editor_path_ext = QtWidgets.QFileDialog.getOpenFileName(self, text.sett_select_text_editor, 'C:\\' if sys.platform == 'win32' else '/')
        path, ext = text_editor_path_ext
        if sys.platform == 'win32':
            if '.exe' not in path:
                QtWidgets.QMessageBox.warning(self, text.warning, text.sett_selected_file_is_not_an_executable)
        self.text_editor_LineEdit.setText(path)
        self.required_action = ACTION_RELOAD
    
    def selectFileManager(self):
        file_manager_path_ext = QtWidgets.QFileDialog.getOpenFileName(self, text.sett_select_file_manager, 'C:\\' if sys.platform == 'win32' else '/')
        path, ext = file_manager_path_ext
        if sys.platform == 'win32':
            if '.exe' not in path:
                QtWidgets.QMessageBox.warning(self, text.warning, text.sett_selected_file_is_not_an_executable)
        self.file_manager_LineEdit.setText(path)
        self.required_action = ACTION_RELOAD
    
    def langChanged(self):
        self.required_action = ACTION_RESTART

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.exec()
    sys.exit(app.exec())