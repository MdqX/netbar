#!/usr/bin/env python3
"""
'mdqxmod' stands for MdqX Module
This module is written by Mateusz "MdqX" Dargacz.
"""

import os
from os import path
import subprocess
import threading
import sys
import re
import logging
import traceback
from glob import glob

__version__ = '1.22.2-indev'

# Takes string and returns boolean value evaluated from string
def boolx(s):
    s = s.lower()
    if re.search('(true|1|y)', s):
        return True
    elif re.search('(false|0|n)', s):
        return False
    else:
        return None


# DEPRECATED (since v1.2)
def inputx(prompt='>'):
    s = input(prompt)
    
    if re.fullmatch(r'^x$', s):
        exit(0)
    else:
        return s


"""
takes dictionary with keys as commands and values as functions (values can be also tuple like: (function, help_text) )
additionally can take dict with escaped commands
also can take a character which is used to escape aforementioned commands
returns inputx (input eXtended) function which works like input() but additionally supports commands

command is treated as a regex pattern in form of a string (it's passed to re.fullmatch)
dictionary with commands supports keys as iterables (tuple recommended) or strings

Example usage of this closure:

def c_mkconf():
    # Some code...

non_esc_cmds = \
{
    'x': lambda: sys.exit()
}
esc_cmds = \
{
    r'(mkconf|makeconfig)': (c_mkconf, 'creates default config file')
}

inputx = new_inputx(non_esc_cmds, esc_cmds)
"""
def new_inputx(non_esc_cmds=None, esc_cmds=None, esc_char='/'):
    def c_help():
        if non_esc_cmds:
            print('Non-escaped commands:')
            for k, v in non_esc_cmds.items():
                if isinstance(v, tuple) or isinstance(v, list):
                    print(f"{k} - {v[1]}")
                else:
                    print(k)
        if esc_cmds:
            print('Escaped commands:')
            for k, v in esc_cmds.items():
                if isinstance(v, tuple) or isinstance(v, list):
                    print(f"{k} - {v[1]}")
                else:
                    print(k)
        
    default_cmds = \
    {
        'x': lambda: sys.exit()
    }
    if non_esc_cmds is None and esc_cmds is None:
        esc_cmds = default_cmds
        non_esc_cmds = {}
    elif non_esc_cmds is None:
        non_esc_cmds = {}
    elif esc_cmds is None:
        esc_cmds = {}
    esc_cmds['help'] = c_help

    def inputx(prompt='>'):
        c = input(prompt)
        had_esc_char = False
        if re.fullmatch(esc_char + r'.+', c):
            c = c.replace(esc_char, '')
            cmds = esc_cmds
            had_esc_char = True
        else:
            cmds = non_esc_cmds
            had_esc_char = False
        
        for command, function in cmds.items():
            if isinstance(command, tuple) or isinstance(command, list):
                for alias in command:
                    if re.fullmatch(re.compile(alias), c):
                        if isinstance(function, tuple) or isinstance(function, list):
                            function[0]()
                        else:
                            function()
                        return None
            elif re.fullmatch(re.compile(command), c):
                if isinstance(function, tuple) or isinstance(function, list):
                    function[0]()
                else:
                    function()
                return None
        if had_esc_char:
            return esc_char + c
        else:
            return c
    return inputx


def make_inputx(non_esc_cmds=None, esc_cmds=None, esc_char='/'):
    def c_help():
        if non_esc_cmds:
            print('Non-escaped commands:')
            for k, v in non_esc_cmds.items():
                if isinstance(v, tuple) or isinstance(v, list):
                    print(f"{k} - {v[1]}")
                else:
                    print(k)
        if esc_cmds:
            print('Escaped commands:')
            for k, v in esc_cmds.items():
                if isinstance(v, tuple) or isinstance(v, list):
                    print(f"{k} - {v[1]}")
                else:
                    print(k)
        
    default_cmds = \
    {
        'x': lambda: sys.exit()
    }
    if non_esc_cmds is None and esc_cmds is None:
        esc_cmds = default_cmds
        non_esc_cmds = {}
    elif non_esc_cmds is None:
        non_esc_cmds = {}
    elif esc_cmds is None:
        esc_cmds = {}
    esc_cmds['help'] = c_help

    def inputx(prompt='>'):
        while 1:
            c = input(prompt)
            had_esc_char = False
            if re.fullmatch(esc_char + r'.+', c):
                c = c.replace(esc_char, '')
                cmds = esc_cmds
                had_esc_char = True
            else:
                cmds = non_esc_cmds
                had_esc_char = False
            
            for command, function in cmds.items():
                if isinstance(command, tuple) or isinstance(command, list):
                    for alias in command:
                        if re.fullmatch(re.compile(alias), c):
                            if isinstance(function, tuple) or isinstance(function, list):
                                function[0]()
                            else:
                                function()
                            # return None
                            break
                elif re.fullmatch(re.compile(command), c):
                    if isinstance(function, tuple) or isinstance(function, list):
                        function[0]()
                    else:
                        function()
                    # return None
                    break
            else:
                if had_esc_char:
                    return esc_char + c
                else:
                    return c
    return inputx


# Clear terminal/CMD STDOUT buffer
def clr():
    # For Windows NT
    if re.fullmatch(r'^(?i)nt$', os.name):
        os.system('cls')
    # For GNU/Linux and Android (Linux shell)
    else:
        os.system('clear')


'''
/About/
setIntervalEx & setTimeoutEx are functions inspired by JavaScript's equivalents without 'Ex' suffix and used in the same manner but
with extended functionality

/Parameters/
func - function to be called
delay - delay between each func call
times - how many times the func is called, if nothing passed or None it will be infinite
first_immediate - if True first func call has no time delay
seconds - if True the specified delay will be treated as amount of seconds

/Example/
# Countdown from 10 to 1 and print('=======BOOM!=======')
from mdqxmod import setIntervalEx, setTimeoutEx

counter = 11
def count():
    global counter
    counter -= 1
    print('--{}--'.format(counter))

setIntervalEx(count, 100, 10)
setTimeoutEx(lambda: print('=======BOOM!======='), 1.1, seconds=True)
'''
def setIntervalEx(func, delay=1000, times=None, first_immediate=False, seconds=False):
    
    if not seconds:
        delay /= 1000
    if times is None:
        i = 1
        while 1:
            t = threading.Timer(delay * i, func)
            t.start()
            i += 1
    elif first_immediate:
        func()
        for x in range(1, times):
            t = threading.Timer(delay * x, func)
            t.start()
    else:
        for x in range(1, times + 1):
            t = threading.Timer(delay * x, func)
            t.start()


def setTimeoutEx(func, delay=1000, seconds=False):
    if not seconds:
        delay /= 1000
    t = threading.Timer(delay, func)
    t.start()


def get_common_browsers():
    APPDATA = os.getenv('appdata')
    common_browsers_win = \
    [
        r'C:\Program Files\Mozilla Firefox\firefox.exe',
        r'C:\Program Files (x86)\Mozilla Firefox\firefox.exe',
        r'C:\Program Files\Google\Chrome\Application\chrome.exe',
        r'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe',
        f'{APPDATA}..\\Local\\Google\\Chrome\\Application\\chrome.exe',
        f'{APPDATA}..\\Local\\Vivaldi\\Application\\vivaldi.exe',
        f"{APPDATA}..\\Local\\\Programs\Opera\launcher.exe"
    ]
    try:
        common_browsers_win.append(glob(r'C:\Windows\SystemApps\Microsoft.MicrosoftEdge*\microsoftedge.exe')[0])
    except IndexError:
        pass

    common_browsers_linux = \
    [
        'firefox',
        'chromium-browser',
        'vivaldi',
        'palemoon'
    ]
    if os.name == 'nt':
        return common_browsers_win
    elif os.name == 'posix':
        return common_browsers_linux
    else:
        return None


"""
By default returns first detected browser.
With get_all_browsers=True returns every detected browsers as dict where key is browser[INDEX] and value is abspath/binary in $PATH
"""
def auto_detect_browser(get_all_browsers=False):
    browser = None
    all_browsers = {}
    browser_index = 0
    
    if os.name == 'nt':
        for _path in get_common_browsers():
            if path.isfile(_path):
                if get_all_browsers:
                    browser_name = None
                    try:
                        browser_name = re.search(r"\\(\w+)[.]exe", _path).group(1)
                    except Exception as exc:
                        print(exc)
                    if browser_name is not None:
                        all_browsers[browser_name] = _path
                    else:
                        all_browsers[f"browser{browser_index}"] = _path
                    browser_index += 1
                else:
                    browser = _path
                    break
    elif os.name == 'posix':
        for i in range(2):
            for binary in get_common_browsers():
                if i == 0:
                    _path = f'/usr/bin/{binary}'
                else:
                    _path = f'/usr/local/bin/{binary}'
                if path.isfile(_path):
                    if get_all_browsers:
                        if binary not in all_browsers.values():
                            all_browsers[f"{binary}{browser_index}"] = binary
                            browser_index += 1
                    else:
                        browser = binary
                        break

    if get_all_browsers:
        return all_browsers
    else:
        return browser


def is_android() -> bool:
    return os.name == 'posix' and path.isfile(path.join('/', 'system', 'build.prop'))


class FSDict:
    """Recursively convert dict or config file into object with attributes & sub-attributes.
Values which are of type dict get converted into FSDict objects (although original dict obj is accessable via *.dict attribute), other values are unchanged.

-- init args:
• dictOrFile - dict to convert or name of the Hjson/JSON file to load and convert

• updateFile - when True, every attribute assignment (with '=' [equals]) will be mirrored in the file specified via 'dictOrFile' or 'fileName' parameter

-- init optional args:
• baseDict - a dict obj which is updated with data from dictOrFile and then becomes the FSDict; useful for default config which ensures that config will contain every expected attribute; when specified and dictOrFile is filename which doesn't exist, there is NO FileNotFoundError

• fileName - if you pass in a dict obj and want to save it, this fileName will be used;
    if fileFormat is not specified, file will be parsed based on this parameter

• fileFormat - JSON, Hjson or YAML (case insensitive), takes precendence over extension derived from fileName

• useDefVal - whether to use defVal kwarg as return value instead of raising KeyError

• defVal - explained above

• sanitise - whether to sanitise dict keys, that is change them in a such way that they can be used as attribute names
    NOTICE! Data with keys which are invalid attribute names can be accessed via *.dict attribute

• fullYAML - uses yaml.BaseLoader instead of SafeLoader which is used by default in FSDict

# Funfact: FS stands for FullStop (a.k.a. dot, period)"""
    _BAD_CHARS = re.compile(r'[-\s]')
    _DIGIT_FIRST = re.compile(r'^\d.*')

    def __init__(self, dictOrFile={}, updateFile=False, fileName=None, fileFormat=None,
        useDefVal=None, defVal=None, sanitise=True, baseDict=None, fullYAML=False):
        self.__dict__['_updateFile'] = updateFile
        self.__dict__['_dictOrFile'] = dictOrFile

        self.__dict__['fileName'] = fileName
        self.__dict__['_fileFormat'] = fileFormat.lower() if fileFormat is not None else None
        self.__dict__['_useDefVal'] = useDefVal
        self.__dict__['_defVal'] = defVal
        self.__dict__['_sanitise'] = sanitise
        self.__dict__['_baseDict'] = baseDict
        self.__dict__['_fullYAML'] = fullYAML
        # kwargs
        """
        self.__dict__['fileName'] = kwargs.get('fileName')
        self.__dict__['_fileFormat'] = kwargs.get('fileFormat', '').lower()
        self.__dict__['_useDefVal'] = kwargs.get('useDefVal', False)
        self.__dict__['_defVal'] = kwargs.get('defVal')
        self.__dict__['_sanitise'] = kwargs.get('sanitise', True)
        self.__dict__['_baseDict'] = kwargs.get('baseDict')
        self.__dict__['_fullYAML'] = kwargs.get('fullYAML', False)
        """

        EXTENSION_PATTERN = re.compile(r'(?:.+?[.])([^.]+)$')

        # file
        if isinstance(dictOrFile, str):
            fileFormat = self._fileFormat
            self.__dict__['_fileExtension'] = EXTENSION_PATTERN.fullmatch(dictOrFile).group(1)
            if not fileFormat:
                fileFormat = self._fileExtension.lower()
            if fileFormat == 'hjson':
                try:
                    import hjson as parser
                except ImportError:
                    import json as parser
            elif fileFormat == 'json':
                import json as parser
            elif re.match(r'ya?ml', fileFormat):
                import yaml
                if not os.path.isfile(dictOrFile) and self._baseDict is not None:
                    dictObj = {}
                else:
                    with open(dictOrFile) as fh:
                        if self._fullYAML:
                            dictObj = yaml.load(fh, Loader=yaml.BaseLoader)
                        else:
                            dictObj = yaml.load(fh, Loader=yaml.SafeLoader)
            else:
                raise Exception(f"Unsupported file extension: {self._fileExtension}")
                
            if not re.match(r'ya?ml', fileFormat):
                if not os.path.isfile(dictOrFile) and self._baseDict is not None:
                    dictObj = {}
                else:
                    with open(dictOrFile) as fh:
                        dictObj = parser.load(fh)

        elif isinstance(dictOrFile, dict):
            if self.fileName is not None:
                self.__dict__['_fileExtension'] = EXTENSION_PATTERN.fullmatch(self.fileName).group(1)
            else:
                self.__dict__['_fileExtension'] = None
            dictObj = dictOrFile
        else:
            raise Exception(f'Expected dict or str, not {type(dictOrFile)}')
        
        if self._baseDict is not None:
            self._baseDict.update(dictObj)
            dictObj = self._baseDict

        self.__dict__['dict'] = dict(dictObj)

        for key, val in dictObj.items():
            digit_first = FSDict._DIGIT_FIRST.match(key)
            # sanitise the key
            if self._sanitise:
                key = FSDict._BAD_CHARS.sub('_', key)
                if digit_first:
                    key = f"_{key}"

            if not FSDict._BAD_CHARS.search(key) and not digit_first:
                if isinstance(val, dict):
                    exec(f"self.{key} = FSDict(val, sanitise={self._sanitise})")
                else:
                    exec(f"self.{key} = {repr(val)}")
            else:
                self.__dict__['dict'][key] = val
    
    def __getattr__(self, name):
        if name not in self.__dict__:
            if name in self.__dict__['dict']:
                return self.__dict__['dict'][name]
            elif self.__dict__['_useDefVal']:
                return self.__dict__['_defVal']
        return self.__dict__[name]

    def __setattr__(self, name, value):
        self.__dict__[name] = value
        if not name.startswith('_') and hasattr(self, 'dict'):
            if isinstance(value, FSDict):
                self.dict[name] = value.dict
            else:
                self.dict[name] = value
            
            if self._updateFile:
                self.save()
    
    def __delattr__(self, name):
        del self.__dict__[name]

        if hasattr(self, 'dict'):
            del self.dict[name]
            
            if self._updateFile:
                self.save()

    def set_tmp(self, name, value):
        self.__dict__[name] = value
    
    def save(self, filename=None):
        used_filename = None
        if self.fileName is not None:
            used_filename = self.fileName
        elif isinstance(self._dictOrFile, str) and filename is None:
            used_filename = self._dictOrFile
        elif filename is not None:
            used_filename = filename
            mo = re.search(r'[.](\w+)$', filename)
            if mo:
                self._fileFormat = mo.group(1)
            else:
                raise Exception('No file extension - cannot determine file format')
        else:
            raise Exception('Filename N/A. Please specify argument "fileName" or\nsetattr(yourFSDict, "fileName", "example.hjson")')
        
        fileFormat = self._fileFormat
        if not fileFormat:
            fileFormat = self._fileExtension

        if fileFormat == 'json':
            import json
            json.dump(self.dict, open(used_filename, 'w'), indent='\t')
        elif re.match(r'ya?ml', fileFormat):
            import yaml
            yaml.dump(self.dict, open(used_filename, 'w'), default_flow_style=False)
        else:
            try:
                import hjson as parser
            except ImportError:
                import json as parser
            parser.dump(self.dict, open(used_filename, 'w'), indent='\t')


def shell(command, no_out=False):
    if '|' in command:
        cmds = command.split('|')
        first = subprocess.Popen(cmds[0].split(), stdout=subprocess.PIPE, universal_newlines=True)
        last = first
        pipes = [first]
        for cmd in cmds[1:]:
            new = subprocess.Popen(cmd.split(), stdin=last.stdout, stdout=subprocess.PIPE, universal_newlines=True)
            pipes.append(new)
            last = new
        pipes[-2].stdout.close()
        
        return last.communicate()[0]
    else:
        if not no_out:
            mo = re.findall(r'[ ]([\'"].*?[\'"]).*', command)
            parts = []
            for p in mo:
                command = command.replace(p, '')
            parts = command.split()
            parts.extend(mo)
            return subprocess.check_output(parts, universal_newlines=True)
        else:
            return os.system(command)

"""
Switch-case-default statement implementation using function.

NOTICE! 'break' keyword is handled ONLY the Python-way. Do not put it outside of the loop.

Format of cases_code:
case 1:
    # a bunch of appropriately indented expressions here...

case 'test':
    # ...

case 3.14159:
    print('PI...almost')

default:
    print('Default case')

It is recommended to pass a raw multiline-string (by prepending 'r' or 'R')
to remove need to escape the escape sequences themselves.

TO DO:
- Support for perl/ruby style regex literals(?), e.g.: /^Reg[eE]xp?/
- Allow same body for multiple cases
- Add optional case-ending 'break' keyword
"""
def switch(x, cases_code: str, _globals={}):
    # convert x (given key) to str for easy multi-datatype support
    x = str(x)

    # dict with keys as switch cases & values as block of code for the specific 'case'
    val_to_func = {}

    # determines for which key the next line of code will be appended to
    key = None

    # create special None key for 'default'
    val_to_func[None] = []
    for line in cases_code.split('\n'):
        # remove leading and trailing whitespace
        line = line.strip()

        # set the key to None for the 'default'
        if line == 'default:':
            key = None
            continue
        # support for single-quoted str
        elif '\'' in line:
            mo = re.fullmatch(r"^(?:(?:case|when)[ ])?'(.*?)':$", line)
        # support for double-quoted str
        elif '"' in line:
            mo = re.fullmatch(r"^(?:(?:case|when)[ ])?\"(.*?)\":$", line)
        # support for float
        elif '.' in line:
            mo = re.fullmatch(r"^(?:(?:case|when)[ ])?(\d+[.]\d+):$", line)
        # support for int
        else:
            mo = re.fullmatch(r"^(?:(?:case|when)[ ])?(\d+):$", line)

        # retrieve the actual value & create list which will contain code ('body') for this specific key ('case')
        if mo:
            key = mo.group(1)
            val_to_func[key] = []
        # append line to the block of code which is executed when x == key ('case body')
        else:
            # support for intuitive indentation of 'case' body
            valid_line = re.sub(r'\t', '', line, count=1)
            val_to_func[key].append(valid_line)

    # execute case-specific code
    if x in val_to_func.keys():
        exec('\n'.join(val_to_func[x]), _globals)
    # execute default case if x is not handled by any specific-case
    else:
        exec('\n'.join(val_to_func[None]), _globals)


'''
exception handling decorator which allows to handle exception even in PyQt5 methods;
exc_block must be a callable which accepts one argument (the exception object).
'''
def exch_factory(exc_block=None, pass_args=False):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            try:
                if pass_args:
                    return func(self, *args, **kwargs)
                else:
                    return func(self)
            except Exception as exc:
                if exc_block is not None:
                    exc_block(exc)
                else:
                    logging.exception(exc)
                    traceback.print_tb(exc.__traceback__)
                    print()
                    print(exc)
        return wrapper
    return decorator


def getch():

	def windows(test_compat=False):
		import msvcrt
		if not test_compat:
			return msvcrt.getch()
	
	def linux():
		import sys, tty, termios
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch
	
	func = windows
	try:
		func(test_compat=True)
	except ImportError:
		func = linux
	finally:
		return func()

def pprint_number(original):
    if len(original) < 4:
        print('INPUT TOO SHORT')
        sys.exit(1)

    rev = ''.join(reversed(original))
    PATTERN = re.compile(r'(?:(\d{3})+?|(\d{1,2}))')
    mo = re.findall(PATTERN, rev)

    main_groups = []
    for group_lst in mo:  # mo[:-1]
        main_groups.append(group_lst[0])

    output_rev = ' '.join(main_groups)
    output_rev = f"{output_rev} {mo[-1][1]}"
    output = ''.join(reversed(output_rev))
    return output

# Testing
if __name__ == '__main__':
    if '--get-version' in sys.argv:
        print(__version__, end='')
        sys.exit()
    elif '-v' in sys.argv:
        print('mdqxmod', __version__)
        sys.exit()

    # d = \
    # {
    #     'root':
    #     {
    #         'sub1':
    #         {
    #             'sub2': 'sub2VAL'
    #         },
    #         'sub1_2': 8
    #     }
    # }

    # cfg = FSDict(d)
    # print(cfg.root.sub1.sub2)

    # import json
    # json.dump({'L1_A': {'L2': {'L3': 8}}, 'L1_B': {'test': '<>'}}, open('test.json', 'w'), indent='\t')
    # Config = FSDict('test.json', updateFile=True)

    # Config.L1_A.L2 = 1
    # print(Config.L1_A.L2)
    # Config.L1_B = 2
    # print(Config.L1_B)
    # Config.another = 'different value'

    # print(shell("uname -a|tr 'x' 'X'|tr '8' ''"))
    #     x = 8
    #     switch(
    #         x,
    #         """
    # case 2:
    #     print('Two')
    # when 4:
    #     print('Four')
    # 8:
    #     print('Eight')
    # default:
    #     print('Default')
    # """
    #     )

    # pydict = \
    # {
    #     'd': {'1': 'test'}
    # }
    # fsd = FSDict(pydict, sanitise=False)
    # print(fsd.d._1)
    # print(getattr(fsd.d, '1'))

    # def c_test():
    #     print('Test')
    
    # def c_test2():
    #     print('Test2')

    # inputx = make_inputx(esc_cmds={'t': c_test}, non_esc_cmds={'t2': c_test2})
    # x = inputx('Val: ')
    # print(x)

    # cfg = FSDict('test.hjson')
    # print(cfg.method)

    # cfg = FSDict('test.csf', fileFormat='yaml')
    # print(cfg.method)

    # DEFAULT_CONFIG = \
    # {
    #     'browser': None,
    #     'check': True,
    #     'lang': 'en'
    # }
    # cfg = FSDict('config.yaml', baseDict=DEFAULT_CONFIG)
    # print(cfg.lang)
    # print(cfg.browser)

    # shell("ls -l '/home/mdqx/Pictures/SMART at 2019-02-07 19-17-01.png'", no_out=True)
