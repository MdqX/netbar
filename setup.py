#!/usr/bin/env python3

import sys
import os
from subprocess import call as run
import json
import argparse

VERSION = '1.0.4'

# args/params
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--mkdep', action='store_true', help='create dependencies.json prefilled with required properties')
parser.add_argument('--get-version', action='store_true', help='used for pnv')
args = parser.parse_args()

if args:
    # make dependencies file
    if args.mkdep:
        dep = \
        {
            "python": [3, 6],
            "modules": [],
            "exec": "",
            "use_apt": True
        }
        json.dump(dep, open('dependencies.json', 'w'), indent='\t')
        print('Dependencies file created')
        sys.exit()
    elif args.get_version:
        print(VERSION, end='')
        sys.exit()

# install dependencies
dependencies = json.load(open('dependencies.json'))

if sys.version_info.major >= dependencies['python'][0] and sys.version_info.minor >= dependencies['python'][1]:
    if sys.platform == 'linux':
        if dependencies["use_apt"]:
            run(('sudo', 'apt', 'update'))
            # ensure that pip & setuptools are present
            run(('sudo', 'apt', 'install', 'python3-pip', 'python3-setuptools'))

    for module in dependencies['modules']:
        if os.name == "nt":
            run(['pip', 'install', module])
        else:
            run(['sudo', 'python{}.{}'.format(sys.version_info.major, sys.version_info.minor), '-m', 'pip', 'install', module])

    if dependencies['exec']:
        os.system(dependencies['exec'])
    print("{} setup is complete".format(os.path.basename(os.path.abspath('.'))))
else:
    print("Error 1: Need Python {}, installed: {}".format('.'.join([str(x) for x in dependencies['python']]), sys.version))
