#!/usr/bin/env python3
# standard libs
import os
import sys
import subprocess
import threading
import datetime
import time
import random
from random import randrange, randint
import re
import json
import traceback
import urllib.parse
import urllib.request
from glob import iglob, glob
from pprint import pprint, pformat
import argparse
from sys import platform
from string import ascii_lowercase

# 3rd party
import hjson
import requests
import bs4 as bs

# own libs
from mdqxmod import auto_detect_browser, FSDict, exch_factory

VERSION = '0.30.0-indev'

# allows using netbox outside of the directory containing it's source file
os.chdir(os.path.dirname(os.path.abspath(__file__)))

DEFAULT_TEXT = \
{
    'error': 'Error',
    'warning': 'Warning',
    'info': 'Info',
    'config_error': 'Error while loading config',
    'using_defaults': 'Using defaults',
    'no_csf': 'No custom search engines detected in csf directory',
    'csf_not_found': 'Unable to find matching CSF (i.e. Search Engine)',
    'csf_dir_na': 'csf directory not found or inaccessible',
    'key_not_found': 'Key not found',
    'no_csf_mapped_to_key': 'no search engine mapped to key',
    'invalid_url_it_must_contain': 'Invalid URL. It must contain',
    'detected_browser': 'Detected browser',
    'other_browsers_available': 'Other browser(s) available check config.',
    'available_search_engines': 'Available search engines',
    'choice': 'Choice',
    'search_with': 'Search with',
    'wrong_index_label_type': 'index/label has unsupported type',
    'text_template_created': 'Text template created',
    'wrong_url_warning': 'Warning: URL might be invalid.',
    'mapped_to_key': 'assigned to index',
    'removed_from': 'Removed from',
    'deleted': 'has been deleted',
    'post_var_not_found': 'Post variable not found',
    # GUI
    'quickadd': 'QuickAdd',
    'url': 'URL',
    'name': 'Name',
    'key': 'Index (int)',
    'add': 'Add',
    'quickadd_manual': """ 
    Search "netbox" using search engine you'd like to add.
    Then paste the URL into the text box.
    'url_from_clipboard': 'URL from clipboard'""",
    'file': 'File',
    'main': 'Main',
    'search_or_visit': 'Search or visit',
    'force_search': 'Force search (ignores URI)',
    'quit': 'Quit',
    'edit': 'Edit',
    'clear_text': 'Clear text',
    'clear_and_paste': 'Clear and paste',
    'show_available_styles': 'Show available styles',
    'focus_on_textbox': 'Focus on text box',
    'save_win_width': 'Save window width',
    'save_win_pos': 'Save window position',
    'show_search_engines_mapping': 'Show search engines mapping',
    'available_styles': 'Available styles',
    'se_keymap': 'SE Keymap',
    'blocked_repeated_search': 'Repeated search has been blocked',
    'force': 'force',
    'updater_n_a': 'Updater is not available',
    'open_config': 'Open config',
    'update': 'Update',
    'restart': 'Restart',
    # Settings
    'sett_settings': 'Settings',
    'sett_title': 'NetBar -> Settings',
    'sett_programmes': 'Programmes',
    'sett_interface': 'Interface',
    'sett_search_engines': 'Search engines',
    'sett_support': 'Support',
    'sett_browser': 'Browser',
    'sett_all_browsers': 'All browsers',
    'sett_text_editor': 'Text editor',
    'sett_file_manager': 'File manager',
    'sett_language': 'Language',
    'sett_use_suggestions': 'Use suggestions',
    'sett_window_style': 'Window style',
    'sett_window_width': 'Window width',
    'sett_window_position': 'Window position',
    'sett_input_font_size': 'Input font size',
    'sett_search_engine_font_size': 'Search engine font size',
    'sett_default_search_engine': 'Default search engine',
    'sett_search_engines': 'Search engines',
    'sett_check_for_update': 'Check for update',
    'sett_error_reporting': 'Error reporting',
    'sett_apply': 'Apply',
    'sett_select': 'Select',
    'sett_select_text_editor': 'Select text editor',
    'sett_select_file_manager': 'Select file manager',
    'sett_selected_file_is_not_an_executable': 'Selected file is not an executable!',
    'sett_settings_applied': 'Settings applied!',
    'sett_close': 'Close',
    'sett_use_loading_animation': 'Use loading animation',
    'sett_use_STOS': 'Use STOS'
}
Text = FSDict(DEFAULT_TEXT)

app = None  # PyQt5 QApplication
win = None  # MainWindow in gfx mode

parser = argparse.ArgumentParser()
SAME_AS_INT_CMD = 'same as NetBar internal command'
parser.add_argument('-g', '--gfx', action='store_true', help='launch NetBar with GUI (DEFAULT) [LEGACY ARG]')
parser.add_argument('--cli', action='store_true', help='launch NetBar with commandline interface')
parser.add_argument('-c', '--command', help='execute NetBar-internal-command')
parser.add_argument('-u', '--url', help='open specified URL')
parser.add_argument('-s', '--search', help='search [text] using default CSF')
parser.add_argument('-S', '--search-with', nargs=2, help='search [text] using [csf]', metavar=('text', 'CSF'))
parser.add_argument('-f', '--from-file', metavar='URL_file', help='reads URL from file and visits that URL')
parser.add_argument('--mkcsf', action='store_true', help=SAME_AS_INT_CMD)
parser.add_argument('--conv-csf', action='store_true', help=SAME_AS_INT_CMD)
parser.add_argument('--no-pyqt5', action='store_true')
parser.add_argument('--no-csf', action='store_true')
parser.add_argument('-d', '--dev', action='store_true', help='run in developer mode')
parser.add_argument('-v', '--version', action='store_true', help='print version')
parser.add_argument('--get-version', action='store_true', help='return version for pnv')
parser.add_argument('-q', '--quit', action='store_true', help='quit after interpreting args (useful with -c)')
args = parser.parse_args()

if args.version:
    print('NetBar', VERSION)
    sys.exit()
if args.get_version:
    print(VERSION, end='')
    sys.exit()

dev = args.dev

pyqt5_available = False
# no_pyqt5 = '--no-pyqt5' in sys.argv[1:]

if not args.no_pyqt5:
    try:
        if dev:
            print('importing PyQt5...')
        from PyQt5 import QtWidgets
        from PyQt5 import QtCore
        from PyQt5 import QtGui
        from PyQt5.QtCore import Qt
        pyqt5_available = True
    except ImportError:
        pass

# Possible update types: NEW|IMPROVED|FIXED|REFACTORED|REMOVED|CHANGED

HELP_TEXT = """
Purpose:
    Search text using specified or default search engine saved in $netbox_dir/csf or visit URL.

Interactive-text-mode:
    This mode supports commands escaped with '/' (forward slash) by default, the escape character can be changed in 'config.hjson'.
    List of these commands is available via './netbox.py -c help' or '/help' in this mode.
    In this mode entering non-command text will result in searching for this text using default search engine.
    You can choose search engine by typing appropriate index and hitting Enter.
    Programme will automatically determine if you've passed URL/domain-name or text to search (thanks to Regular Expressions)

Acronyms:
    CSF - Custom Search File
    SE - Search Engine
    SEC - Search Engine Code
    DEF_SE - Default Search Engine
    EID - Error ID

"""

CONFIG_FILE = os.path.join('data', 'config.hjson')
HISTORY_FILE = os.path.join('data', 'history.hjson')

# contains configuration for the program
Config = None
DEFAULT_CONFIG = \
{
    'browser': None,
    'def_se': 'DDG',
    'clear_before_menu': False,
    'sort_labels_by_keys': True,
    'use_msgbox': False,
    'cmd_char': '/',
    'map_every_csf': True,
    'se_keymap': {"Google": 0},
    'EID': random.randint(11, 1024),
    'text_editor': 'pluma' if sys.platform == 'linux' else 'notepad.exe',
    'file_mgr': 'caja' if sys.platform == 'linux' else 'explorer.exe',
    'use_suggestions': True,
    'text': 'en',
    'check_for_updates': False,
    'gui_style': None,
    'gui_width': 385,
    'gui_position': None,
    'gui_text_box_font_size': 13,
    'gui_csf_box_font_size': 13,
    'gui_pl100': False,
    'block_repeated_search': True,
    'try_focus_prev_instance': False,
    'use_STOS': False,
    'use_loading_anim': True,
    'gui_bg_colour': None,
    'use_updater': False
}
history = []

csf_label_filepath = {}  # dict {csf_label: csf_filepath}
csf_key_label = {}  # dict {int(key): str(csf_label)}

Text = None

no_config = False
local_config_only = False

previous_input = '[PREV_NULL]'  # the previously entered text as a parameter to inputx() function
current_input = '[CURR_NULL]'  # the last entered text as a parameter to inputx() function

# `patterns
# https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top <- good
# http://some.lad@www.something.org:21/stuff/things/idk/?use=new-cool-stuff&order=newest#bottom <- good
# http://some.lad@www.something.org:80000/stuff/things/idk/?use=new-cool-stuff&order=newest#bottom <- wrong port
# https://search.yahoo.com/search?p=netbox&fr=yfp-t&fp=1&toggle=1&cop=mss&ei=UTF-8 <- good
URL_PATTERN = \
r'^(?P<baseaddr>(https?:\/\/)?(\w+[.]\w+@)?(www[.])?(?P<domain>([\w-]+[.])+)[\w-]+)(:(?P<port>\d+))?(\/[^\/]+)*\/?[?]?([^\/]+=[^\/]+)?(&[^\/]+=[^\/]+)*(#[^\/]+)?$'
LOCAL_FILE_PATTERN = r'^file://.+$'
LOCALHOST_URL_PATTERN = r'localhost(/[\w~-]+)*?/?'
IPV4_PATTERN = r'^(\d{1,3})[.](\d{1,3})[.](\d{1,3})[.](\d{1,3})(?:[:](\d{1,5}))?$'

URI_PATTERN = re.compile(f"({URL_PATTERN}|{LOCAL_FILE_PATTERN}|{LOCALHOST_URL_PATTERN})")

launched_gfx_mode = False

# `lcfg
def load_config(cfg_file=CONFIG_FILE, return_config=False):
    global Config, csf_key_label

    if os.path.isfile(cfg_file):
        try:
            if return_config:
                return FSDict(CONFIG_FILE, baseDict=DEFAULT_CONFIG, updateFile=True, sanitise=False)
            else:
                Config = FSDict(CONFIG_FILE, baseDict=DEFAULT_CONFIG, updateFile=True, sanitise=False)
        except Exception as exc:
            global local_config_only
            local_config_only = True
            Config = FSDict(DEFAULT_CONFIG, updateFile=True, sanitise=False, fileName=CONFIG_FILE)
            print(f"{Text.config_error}:")
            traceback.print_tb(exc.__traceback__)
            print(f"\n{Text.using_defaults}")
        # reset
        csf_key_label = dict()

        if Config.se_keymap is not None:
            for k, v in Config.se_keymap.dict.items():
                csf_key_label[v] = k

        # Add missing CSFs
        if Config.map_every_csf and os.path.isdir('csf'):
            csf_dir = (csf for csf in os.listdir('csf') if re.fullmatch(r'.+\.json', csf))
            labels = set()
            for csf_filename in csf_dir:
                data = json.load(open(os.path.join('csf', csf_filename)))
                labels.add(data['label'])

            for label in labels:
                if label not in csf_key_label.values():
                    index = 0
                    while index in csf_key_label.keys():
                        index = randrange(2 * len(csf_key_label.items()))
                    csf_key_label[index] = label
    else:
        if return_config:
            return FSDict(DEFAULT_CONFIG, updateFile=True, sanitise=False, fileName=CONFIG_FILE)
        OLD_CONFIG_FILE = 'config.yaml'
        # convert old config to new format
        if os.path.isfile(OLD_CONFIG_FILE):
            try:
                import yaml
                cfg = yaml.load(open(OLD_CONFIG_FILE))
                hjson.dump(cfg, open(CONFIG_FILE, 'w'), indent='\t')
                if dev:
                    print('Config has been converted')
            except ImportError:
                print(f'Cannot autoconvert config.yaml to {CONFIG_FILE}: missing pyyaml module', 1)
                create_config()
        else:
            create_config()
        reload()

def create_config():
    try:
        # backup current config
        conf = CONFIG_FILE
        conf_backup = f'{CONFIG_FILE}.backup'
        # remove old backup if exists
        if os.path.isfile(conf):
            if os.path.isfile(conf_backup):
                os.remove(conf_backup)
            os.rename(conf, conf_backup)

        # create fresh cfg
        with open(CONFIG_FILE, 'w') as f:
            json.dump(DEFAULT_CONFIG, f, indent='\t')
    except IOError as exc:
        msg(exc, 1)


def edit_file(file):
    te = Config.text_editor
    # when using text editor in terminal (e.g. mate-terminal -e "vim FILE"), add quotation mark at the end
    if re.search('%[qQ]$', te):
        te = te[:-2]
    cmd = f'{te} {file}'
    if Config.text_editor.endswith('%q'):
        cmd += "'"
    elif Config.text_editor.endswith('%Q'):
        cmd += '"'
    os.system(cmd)


def open_config():
    def thread_func():
        edit_file(CONFIG_FILE)
    threading.Thread(target=thread_func).run()


def load_text(_config=Config, return_text=False):
    text_file = None
    if (_config.text is not None) and (_config.text not in ('en', 'default')):
        if '.yaml' in _config.text:
            text_file = os.path.join('text', _config.text)
        else:
            text_file = os.path.join('text', f"{_config.text}.yaml")
        if os.path.isfile(text_file):
            if not return_text:
                global Text
                Text = FSDict(text_file)
    else:
        Text = FSDict(DEFAULT_TEXT)
    if return_text:
        if _config.text in ('en', 'default'):
            return FSDict(DEFAULT_TEXT)
        else:
            return FSDict(text_file)


# `lclf |creates a dict storing label & filepath of search engines which are stored in csf directory
def load_csf_label_filepath(csf_dir='csf'):
    if os.path.isdir(csf_dir):
        # reset csf_label_filepath
        global csf_label_filepath
        csf_label_filepath = {}

        csf_files = glob(os.path.join(csf_dir, '*.json'))
        if len(csf_files) == 0:
            msg(Text.no_csf, 2, terminal_only=True)
        else:
            for index, file in enumerate(csf_files):
                csf_filepath = file
                csf_label = json.load(open(csf_filepath))['label']
                csf_label_filepath[csf_label] = csf_filepath

                # Auto-create (csf_label <-> key) mapping
                if no_config:
                    csf_key_label[index] = csf_label
    else:
        print('Warning: CSF dir missing')


# `lcsf |loads JSON data from CSF file with specified label and returns these data
def load_csf(csf_label=None, use_regex=False):
    if csf_label is None:
        csf_label = Config.def_se
    if csf_label.upper() == 'DDG':
        data = \
        {
            "url": "https://duckduckgo.com/?q=",
            "label": "DDG",
            "method": 'get',
            'post_var': None
        }
    elif csf_label.upper() == 'GOOGLE':
        data = \
        {
            "label": "Google",
            "url": "https://www.google.pl/search?source=hp&ei=l68AX-f2M7CMrwSUpaXwAw&q=$&oq=$&gs_lcp=CgZwc3ktYWIQAzICCAAyAggAMgIIADICCAAyAggAMgQIABAKMgIIADIECAAQCjICCAAyAggAUPcJWMoOYIIQaAFwAHgAgAGXAYgBwAWSAQM0LjOYAQCgAQGqAQdnd3Mtd2l6sAEA&sclient=psy-ab&ved=0ahUKEwinntu6g7TqAhUwxosKHZRSCT4Q4dUDCAY&uact=5",
            "method": "get",
            "post_var": None
        }
    else:
        if use_regex:
            path = None
            for label, filepath in csf_label_filepath.items():
                if re.search(csf_label, label):
                    path = csf_label_filepath[label]
                elif re.search(csf_label, filepath):
                    path = filepath
            if path is None:
                raise Exception(f"load_csf: {Text.csf_not_found}")
        else:
            path = str(csf_label_filepath[csf_label])

        path = path.replace('\n', '')
        data = json.load(open(path))
    return data


def make_csf():
    new_csf = \
    {
        'label': '',
        'url': '',
        'method': 'get',
        'post_var': ''
    }
    json.dump(new_csf, open(os.path.join('csf', 'new.json'), 'w'), indent='\t')


def convert_csf():
    try:
        import hjson
    except ImportError:
        pass

    def conv(file):
        try:
            csf = hjson.load(open(file))
        except ModuleNotFoundError:
            try:
                csf = json.load(open(file))
            except json.JSONDecodeError as exc:
                traceback.print_tb(exc.__traceback__)
                msg('The CSF file probably has Hjson format, installing "hjson" via pip should resolve this issue.', 1)
                return
        if 'method' not in csf:
            csf['method'] = 'get'
        if 'post_var' not in csf:
            csf['post_var'] = None
        if 'sec' in csf:
            csf['url'] = csf['sec']
            del csf['sec']
        if 'sr' in csf:
            del csf['sr']
        if 'rsr' in csf:
            del csf['rsr']
        outfile = re.sub(r'[.].*$', '.json', file)
        print('\u2022', outfile)
        json.dump(csf, open(outfile, 'w'), indent='\t')

    for file in iglob(os.path.join('csf', '*json')):
        conv(file)
    for file in iglob(os.path.join('csf', '*.csf')):
        conv(file)

def gen_url(text, csf):
    search_engine_code = csf['url']

    encoded_text = urllib.parse.quote(text)
    # allows to keep the URL part on the right side (required for some search engines)
    if '$' in search_engine_code:
        url = search_engine_code.replace('$', encoded_text)
    else:
        url = search_engine_code + encoded_text
    url = url.replace('\n', '')
    return url

# `search
'''
text - searched text
csf - custom search format data
gfx_mode - graphical mode
exit_when_done - close the programme after execution of this function
multi - MultiSearch(TM) mode
'''
def search(text='test', csf=None, gfx_mode=False, exit_when_done=False, multi=False):
    # load default search engine
    if csf is None:
        csf = json.load(open(csf_label_filepath[Config.def_se]))
    
    if csf['method'] == 'multisearch' or multi:
        csf_list = []
        csf_list.append(load_csf('Google'))
        csf_list.append(load_csf('Bing'))
        csf_list.append(load_csf('Yahoo'))

        urls = []

        for csf in csf_list:
            urls.append(gen_url(text, csf))
        
        # Google
        headers = {'user-agent': '''Mozilla/5.0 (Linux; 10.0.0; Moto E5 Plus Build/XT1924-1) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/91.0.4472.164 Mobile Safari/535.19'''}
        soup = requests.get(urls[0], headers=headers).text
        site = bs.BeautifulSoup(soup, 'lxml')
        links_google = site.find_all('a')
        for lnk in links_google[:]:
            if text.lower() not in str(lnk).lower():
                links_google.remove(lnk)

        # Bing
        soup = requests.get(urls[1], headers=headers).text
        site = bs.BeautifulSoup(soup, 'lxml')
        links_bing = site.find_all('a')
        for lnk in links_bing[:]:
            if text.lower() not in str(lnk).lower():
                links_bing.remove(lnk)

        # Yahoo
        soup = requests.get(urls[2], headers=headers).text
        site = bs.BeautifulSoup(soup, 'lxml')
        links_yahoo = site.find_all('a')
        for lnk in links_yahoo[:]:
            if text.lower() not in str(lnk).lower():
                links_yahoo.remove(lnk)

        html = \
        """
        
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta lang="en">
    <style>
        #main_header
        {{
            font-size: 30px;
            width: 100%;
            color: white;
            background-color: green;
        }}
        .header_style
        {{
            font-size: 25px;
            width: 100%;
            color: white;
            background-color: blue;
        }}
    </style>
</head>
<body>
    <div id="main_header">NetBar MultiSearch</div>
    <div class="header_style">Google</div>
    {0}
    <div class="header_style">Bing</div>
    {1}
    <div class="header_style">Yahoo</div>
    {2}
</body>
</html>
        """.format(links_google, links_bing, links_yahoo)
        MULTISEARCH_HTML = "multisearch.html" 
        with open(MULTISEARCH_HTML, 'w') as fh:
            fh.write(html)
        init_browser(MULTISEARCH_HTML, gfx_mode=gfx_mode, exit_when_done=exit_when_done)
    elif csf['method'].lower() == 'get':
        url = gen_url(text, csf)
        # print(url)  # TMP!
        init_browser(url, gfx_mode=gfx_mode, exit_when_done=exit_when_done)
    elif csf['method'].lower() == 'post':
        from os.path import join

        storage = join('csf', 'web_content', csf['label'].lower().replace(' ', '_'))
        if not os.path.isdir(storage):
            os.makedirs(storage)
        main_html = join(storage, 'main.html')
        base_url = re.match(URL_PATTERN, csf['url']).group('baseaddr')

        r = requests.post(csf['url'], data={csf['post_var']: text})

        main_lines = r.text.split('<')
        main_lines = ['<' + x for x in main_lines]
        IMG_OR_LINK_SRC = re.compile(r'(?:url\(\'(.+?)\'\)|<(?:img|link|script).+?(?:src|href)="(.+?)")')
        A_HREF = re.compile(r'<a.+?href="(.+)"')

        try:
            for i, line in enumerate(main_lines):
                # fix links so they point to remote page which is not available locally
                mo = A_HREF.search(line)
                if mo:
                    href = mo.group(1)
                    if not href.startswith(base_url):
                        sep = '/'
                        if href.startswith('/'):
                            sep = ''
                        main_lines[i] = line.replace(href, f"{base_url}{sep}{href}")
                mo = IMG_OR_LINK_SRC.search(line)
                if mo:
                    res_url = mo.group(1)
                    if not res_url:
                        res_url = mo.group(2)
                    if res_url.startswith('/'):
                        # fix path by removing preceding slash
                        main_lines[i] = main_lines[i].replace(res_url, res_url[1:])

                        src = base_url + res_url
                        if not re.match(URL_PATTERN, src) or os.path.isfile(join(storage, res_url[1:])):
                            continue
                        if dev:
                            print('GET', src, sep=' \t')
                        try:
                            r = requests.get(src)
                        except Exception as exc:
                            print(f'Error: could not GET "{src}"')
                            print(f"Details:\n{exc}")
                            continue
                        if dev:
                            print(r.status_code, r.reason, '\n', sep=' \t')
                        
                        res_url = res_url.replace('/', os.path.sep)
                        res_url = res_url[1:]
                        res_dir = os.path.dirname(res_url)
                        if not os.path.isdir(join(storage, res_dir)):
                            os.makedirs(join(storage, res_dir))
                        with open(join(storage, res_url), 'wb') as fh:
                            fh.write(r.content)
        finally:
            with open(main_html, 'w') as fh:
                fh.writelines(main_lines)
            init_browser(main_html, gfx_mode=True)

def is_ipv4(text):
    if re.fullmatch(IPV4_PATTERN, text):
        octs_correct = True
        port_correct = True

        matches = re.findall(IPV4_PATTERN, text)
        octs = matches[0][:3]
        port = None
        if len(matches[0]) == 5:
            port = matches[0][4]

        for octet in octs:
            if not octet.isnumeric():
                octs_correct = False
            elif int(octet) > 255 or int(octet) < 0:
                octs_correct = False

        if port is not None:
            if not port.isnumeric():
                port_correct = False
            elif int(port) > 65535 or int(port) < 0:
                port_correct = False

        if octs_correct and port_correct:
            return True
    return False

def is_uri(text: str):
    do_search = False
    mo = URI_PATTERN.fullmatch(text)
    if mo is not None:
        try:
            port = mo.group('port')
            if port is not None:
                if int(port) not in range(65536 + 1):
                    do_search = True
        except IndexError:
            pass

    return (URI_PATTERN.fullmatch(text) or is_ipv4(text)) and not re.fullmatch(f'{Config.cmd_char}search .+', text) and not do_search

# Auto-detect URL or IPv4 & depending on detected form either search input or visit webpage
def search_or_visit(text, exit_when_done=False):
    if is_uri(text):
        init_browser(text, exit_when_done=exit_when_done)
    else:
        text = re.sub(r'search\s', '', text, count=1)
        csf = load_csf()  # use default search engine
        search(text, csf, exit_when_done=exit_when_done)  # search for entered string

def command_interpreter(c, escape=False):
    if escape:
        c = Config.cmd_char + c

    if c == '' or not c.startswith(Config.cmd_char):
        return 1

    c = c[1:]  # remove first char

    # for multiple commands simultaneously
    if ';' in c:
        cmds = c.split(';')
        for command in cmds:
            command_interpreter('{0}'.format(command), escape=True)

    elif c.startswith('browser='):
        browser = c.split('=')[1]
        Config.set_tmp('browser', browser)

    elif c.startswith('url '):
        url = c.split(' ')[1]
        init_browser(url, gfx_mode=launched_gfx_mode)
    elif c == 'settings':
        subprocess.run(('pythonw', 'settings.py'))
    elif c == 'update':
        if Config.use_update:
            try_update(VERSION, pyqt5_ready=launched_gfx_mode)
        else:
            msg(Text.updater_n_a, 1)
    elif c == 'update -f':
        if Config.use_update:
            try_update(VERSION, force=True, pyqt5_ready=launched_gfx_mode)

    elif c.startswith('rm ') or c.startswith('remove '):
        q = re.match(r'(?:rm|remove)\s([\w\s]+)$', c)
        if q:
            q = q.group(1)
        else:
            msg('Incorrect syntax')
            return 0

        try:
            if q.isnumeric():
                key = int(q)
                filepath = csf_label_filepath[csf_key_label[key]]
                label = csf_key_label[key]
                del Config.se_keymap.dict[label]
                Config.save()
                # del Config.se_keymap.dict[label]
                if dev:
                    msg(f'{label} {Text.removed_from} {CONFIG_FILE}')
                # remove *.json file
                os.remove(filepath)
                if dev:
                    msg(f'{label} {Text.removed_from} CSF')
                else:
                    msg(f'{label} {Text.deleted}')
            else:
                label = q
                del Config.se_keymap.dict[label]
                Config.save()
                # del Config.se_keymap.dict[label]
                if dev:
                    msg(f'{label} {Text.removed_from} {CONFIG_FILE}')
                filepath = csf_label_filepath[label]
                os.remove(filepath)
                if dev:
                    msg(f'{label} {Text.removed_from} CSF')
                else:
                    msg(f'{label} {Text.deleted}')
        except AttributeError as ae:
            traceback.format_tb(ae.__traceback__)
            print(ae)

        except KeyError as ke:
            # msg(f'rm: {Text.csf_not_found}: {q}', 1)
            traceback.print_tb(ke.__traceback__)
            print(ke)
        # finally:
        # 	restart()

    elif c.startswith('ls ') or c.startswith('listdir ') or c == 'ls':
        output = str()
        if c == 'ls':
            directory = '.'
        else:
            directory = c.split(' ')[1]
        for x in os.listdir(directory):
            output += '{}\n'.format(x)
        msg(output)
            
    elif c.startswith('val '):
        matches = re.findall(r'cfg[.](\w+)', c)
        if(len(matches) > 0):
            msg(f"Config.{matches[0]}:\n{getattr(Config, matches[0], 'Not found')}")
        elif re.search(r'val\s.*', c):
            var = re.findall(r'val\s(\w+)', c)[0]
            msg(f"{matches[0]}:\n{globals()[var]}")
        else:
            print('Variable "{}" not found'.format(c))
    elif c == 'val':
        for i in globals().items():
            print(i)
    elif c.startswith('eval '):
        code = re.match(r'eval (.*)', c).group(1)
        msg(str(pformat(eval(code), width=1)))
    elif c == 'mkcsf' or c == 'make_csf' or c == 'mcsf':
        make_csf()

    elif c == 'convert_csf':
        convert_csf()

    elif c == 'quick_add' or c == 'qa':
        # get values
        print('1. Search "netbox" using the search engine you want to add\n')
        print('2. Next paste the URL you achieved by doing step 1\n')
        url = inputx('URL (skip=use text from clipboard):')

        # Auto-retrieve URL from Clipboard
        if url == '':
            if pyqt5_available:
                app = QtWidgets.QApplication([])
                clipboard = app.clipboard()
                url = clipboard.text()
                app.quit()
                print('URL = {}'.format(url))
            else:
                msg('\nUnable to auto-retrieve text from clipboard, PyQt5 framework not found.')
                url = inputx('Enter or paste the URL:\n')
        # url = url.replace(';', '')

        print('\n3. Name this search engine whatever you want')
        label = inputx('Name (skip=auto-detect): ')

        print()
        print('4. Enter number which will be used to choose this search engine')
        key = inputx('Key (skip=random): ')

        quick_add(url, label, key)

    elif c == 'mkconf':
        create_config()

        msg('config created!')

    elif re.fullmatch(r'^restoreconf|rsconf$', c):
        if os.path.isfile(f'{CONFIG_FILE}.backup'):
            safety_check = inputx('Current config is going to be permanently deleted.\nAre you sure? (Yes, I am sure):\n')
            if safety_check == 'Yes, I am sure':
                try:
                    os.remove(CONFIG_FILE)
                    os.rename(f'{CONFIG_FILE}.backup', CONFIG_FILE)
                    msg('Config restored successfully')
                except IOError as exc:
                    msg(exc, 3, 'Error -> Restore config')
            else:
                print('Aborted')
        else:
            print('Backup not found')

    elif c == 'backup_his':
        backup_history()
    
    elif c == 'restore_his':
        restore_history()
    elif c == 'load_conf':
        load_config()
    elif c == 'load_csf_lf':
        load_csf_label_filepath()
    elif c == 'load_his':
        load_history()

    elif c == 'msgbox' or c == 'mb':
        t = inputx('Text:\n')
        header_type = inputx('Type (0-3): ')
        if int(header_type) < 0 or int(header_type) > 3:
            msg('Unsupported header_type ({0})'.format(header_type), 1)
            return 0

        custom_header = 'CUSTOM'
        manual_fix = 0
        if header_type == '3':
            custom_header = inputx('Custom header: ')

            manual_fix = inputx('Manual fix (int): ')
        print()
        msgbox(t, int(header_type), custom_header, int(manual_fix))

    elif c == 'meme':
        t = """
There's some issue with the code?
Can't find the solution?
Try adding "global [var]" before setting var's value
"""
        msg(t[1:], 3, 'Global-keyword-meme', 1)

    elif re.fullmatch(r'(gui|gfx|window)', c):
        launch_gfx()

    elif c == 'menu' or c == 'm':
        menu()

    elif c == 'reload' or c == 'rl':
        reload()

    elif c == 'exit' or c == 'x':
        print()
        exit(0)
    # clear & cls is for conservative people, I use '#c' (almost c# xD)
    elif c == 'clear' or c == 'cls' or c == 'c' or c == 'clr':
        clear()

    elif c == 'c -n' or c == 'clr -n':
        clear(True)

    elif c == 'cfg':
        open_config()

    elif c == 'his' or c == 'history':
        def thread_func():
            edit_file(HISTORY_FILE)
        threading.Thread(target=thread_func).run()

    elif c == 'data':
        data_dir = os.path.abspath(os.path.dirname(__file__))

        def thread_func():
            os.system(f'{Config.file_mgr} "{data_dir}"')

        threading.Thread(target=thread_func).run()

    elif c == 'detect_browser':
        detect_browser()

    # `test
    elif c == 'test' or c == 't':
        msg('Test')

    # `help
    elif c == 'help':
        int_cmds_help_text = f"""
-- Internal commands --
1. CLI user:
qa, quick_add - starts quick-add wizard to add new search engine
rm [csf_label], remove [...] - removes search engine specified by label
rl, reload - reloads config, list of search engines & text
update [-f] - updates NetBar if newer version is available; -f = force update
cfg - opens {CONFIG_FILE} using {Config.text_editor}
his, history - opens {HISTORY_FILE} in {Config.text_editor}
gui, gfx, window - starts graphical mode
cl [-f], changelog [-f] - prints changelog for current version of NetBar; with -f opens change.log in {Config.text_editor}
help - you know this one already!
m, menu - takes you to menu
r, redo - uses/interprets previously entered text
c, clear, cls - cleans terminal/CMD buffer
"c -n" - prints 30 new lines to pseudo-clear
res, restart - restarts NetBar [Beta]
x, exit - exit

2. Power user:
url [URL] - opens specified URL (useful if entered URL is searched instead of opened)
browser=[browser's path] - changes used browser
detect_browser - sets Config.browser value to auto detected browser path
mkconf - saves default config to "{CONFIG_FILE}"and keeps old config file as "{CONFIG_FILE}.old"
rsconf, restoreconf - restores config backup
backup_his - dumps history from system memory to separate file with .backup suffix
restore_his - overwrites current history file with backup history file
mkcsf, make_csf, mcsf - creates csf/new.json file with required properties inside
convert_csf - converts every legacy CSF to the current version of CSF format
load_conf - loads {CONFIG_FILE}
load_csf_lf - loads csf_label_filepath mapping
load_his - loads {HISTORY_FILE}

3. Developer:
t, test - executes test command (may vary depending on version)
[DEPRECATED] val - pseudo-function which takes 0 or 1 parameter and returns either list of globals or value when name of global is specified;
    if prefixed with 'cfg.X' it will return value of Config.X, e.g.: val cfg.browser # output could be 'firefox'
eval - evaluates code to string and shows it with msg()
prev_value - prints value of previous_input variable
ls [dir], listdir [dir] - displays list of files inside specified directory
mb, msgbox - starts TextMessageBox wizard (header types: 0 = INFO, 1 = ERROR, 2 = WARNING, 3 = CUSTOM)
meme - shows msgbox with the global-keyword-meme
"""
        

    # `cl `CHANGELOG
    elif c == 'changelog' or c == 'cl':
        if os.path.isfile('change.log'):
            changelog_text = [f'{VERSION}:']
            full_changelog = open('change.log').readlines()
            start_index = 0
            for index, line in enumerate(full_changelog):
                if re.match(f"{VERSION}.*", line):
                    start_index = index + 1
                    break
            else:
                msg('Changelog is N/A for this version.')
            
            changelog_text.extend(full_changelog[start_index:])
            msg('\n'.join(changelog_text), 3, 'CHANGELOG', 0)
        else:
            msg('Missing "change.log" file', 1)

    elif c == 'changelog -f' or c == 'cl -f':
        CHANGELOG_FILE = 'change.log'

        def thread_func():
            edit_file(CHANGELOG_FILE)

        if os.path.isfile(CHANGELOG_FILE):
            threading.Thread(target=thread_func).start()

    elif c == 'error' or c == 'exception':
        raise Exception('Test')
    
    elif c == 'res' or c == 'restart':
        restart()

    else:
        return 1
    return 0


def quick_add(url, label, key):
    if not URI_PATTERN.fullmatch(url):
        msg(Text.wrong_url_warning, 2)

    method = 'get'
    post_var = None

    # get search engine code from given URL/domain address
    mo = re.match(r'(.+)netbox(.*)', url)
    # method GET
    if mo:
        sec = f"{mo.group(1)}$"  # the '$' represents the searched string passed to search engine
        if len(mo.groups()) > 1:
            sec += mo.group(2)
    # method POST
    else:
        # msg(f'{Text.invalid_url_it_must_contain} "netbox".')
        # return
        sec = url
        method = 'post'
    
    if method == 'post':
        # get the post_var from form input
        html = requests.get(url).text

        # with open('post_search.html', 'w') as fh:
        # 	fh.write(html)
        mo = re.search(r'(?si)<form.+?action=".+?search.+?<input.+?name="(\w+?)"', html)
        if mo:
            post_var = mo.group(1)
        else:
            msg(Text.post_var_not_found, 1)
            return None

    if not label:
        # get the domain name
        label = ' '.join(reversed(re.fullmatch(URL_PATTERN, url).group('domain').split('.')))
        msg('Name = {}'.format(label))
    
    if not key:
        key = randint(0, 100)
        while key in csf_key_label.keys():
            key = randint(0, 100)
        msg('Key = {}'.format(key))
    elif not key.isnumeric() and len(key) == 1:
        key = ord(key)

    # create CSF file based on acquired data
    new_csf = \
    {
        'label': label,
        'url': sec,
        'method': method,
        'post_var': post_var
    }
    filename = '{}.json'.format(label.lower().replace(' ', '_'))

    with open(os.path.join('csf', filename), 'w') as f:
        json.dump(new_csf, f, indent='\t')

    # update cfg
    setattr(Config.se_keymap, label, int(key))
    Config.save()
    reload()

    msg(f'{label} {Text.mapped_to_key} {key}')
    restart()


def restart():
    args = sys.argv

    if sys.platform == 'linux':
        os.execl(sys.executable, sys.executable, *args)
    elif sys.platform == 'win32':
        def func():
            prefix = os.path.basename(sys.executable)

            args.insert(0, prefix)
            subprocess.run(args)

        threading.Thread(target=func, daemon=True).start()
        sys.exit()
        

def detect_browser():
    browser = auto_detect_browser()
    if browser is None:
        browser = ''
    Config.browser = browser
    print(f'{Text.detected_browser}: {browser}')

    all_browsers = auto_detect_browser(get_all_browsers=True)
    # pprint(all_browsers)
    if len(all_browsers) > 1:
        Config.browsers = all_browsers
        msg(Text.other_browsers_available)

def init_browser(url, gfx_mode=False, exit_when_done=False):
    def run_browser():
        browser = Config.browser
        print(browser)
        # if Config.browser is alias
        mo = re.match(r'\$(\w+)', browser)
        if mo:
            browser = Config.browsers.dict[mo.group(1)]
        
        subprocess.call([browser, url])
    
    url = re.sub(r'\n', '', url)

    if os.name == 'posix' and exit_when_done:
        pid = os.fork()
        if pid == 0:
            run_browser()
        sys.exit()
    else:
        th = threading.Thread(target=run_browser)
        th.daemon = True
        th.start()

def clear(use_newlines=False):
    if use_newlines:
        print('\n' * 30)
    else:
        command = ''
        if os.name == 'nt':  # if os is Windows
            command = 'cls'
        else:
            command = 'clear'
        os.system(command)


# `menu
def menu():
    while 1:
        if Config.clear_before_menu:
            command_interpreter('c', escape=True)

        print('---------------------------------')
        print('NetBar {0} coded by MdqX'.format(VERSION))
        print('---------------------------------')

        print(f'{Text.available_search_engines}:')
        print()
        if Config.sort_labels_by_keys:
            se_sorted_list = []
            for k, v in csf_key_label.items():
                se_sorted_list.append('[{}] {}'.format(k, v))
            se_sorted_list.sort()
            for i in se_sorted_list:
                print('{}\n'.format(i))
        else:
            for k, v in csf_key_label.items():
                print('[{}] {}\n'.format(k, v))

        choice = inputx(f'\n{Text.choice}: ')
        if not choice.isnumeric():
            add_to_history(choice)
            search_or_visit(choice)
        else:
            choice = int(choice)
            csf = None
            if choice in csf_key_label.keys():
                csf = load_csf(csf_key_label[choice])
            else:
                print(f'{Text.error}: {Text.no_csf_mapped_to_key} {choice}')
                inputx()
                continue

            text = inputx(f'{Text.search_with} {csf_key_label[choice]}: ')
            add_to_history(text)
            search(text, csf)


def reload():
    load_config()
    load_text(Config)
    if not args.no_csf and not args.conv_csf:
        load_csf_label_filepath()
    load_history()

def get_datetime():
    d = datetime.date.today()
    datetime_obj = datetime.today()

    time_tuple = datetime_obj.timetuple()
    h = time_tuple[3]
    if h < 10:
        h = '0{0}'.format(h)
    m = time_tuple[4]
    if m < 10:
        m = '0{0}'.format(m)
    s = time_tuple[5]
    if s < 10:
        s = '0{0}'.format(s)

    t = '{0}:{1}:{2}'.format(h, m, s)
    dt = '{0} {1}'.format(d, t)
    return dt

# header based on type
def make_msgbox_header(top_len, header_type, custom_header='CUSTOM', manual_fix=0):
    header = "Header's default value"
    if header_type == 0:
        pushers_left = ' ' * ((top_len // 2) - 2)
        if (top_len % 2) == 0:
            pushers_right = ' ' * (top_len // 2 - 2)
        else:
            pushers_right = ' ' * (top_len // 2 - 1)
        header = '|| {0}INFO{1} ||'.format(pushers_left, pushers_right)
    elif header_type == 1:
        if top_len < 5:
            top_len = 5
        pushers_left = ' ' * ((top_len // 2) - 3)
        if (top_len % 2) == 0:
            pushers_right = ' ' * (top_len // 2 - 2)
        else:
            pushers_right = ' ' * (top_len // 2 - 1)
        header = '|| {0}ERROR{1} ||'.format(pushers_left, pushers_right)
    elif header_type == 2:
        if top_len < 7:
            top_len = 7
        pushers_left = ' ' * ((top_len // 2) - 4)
        if (top_len % 2) == 0:
            pushers_right = ' ' * (top_len // 2 - 3)
        else:
            pushers_right = ' ' * (top_len // 2 - 2)
        header = '|| {0}WARNING{1} ||'.format(pushers_left, pushers_right)
    # custom header
    elif header_type == 3:
        pushers_left = ' ' * ((top_len // 2) - (len(custom_header) // 2))

        fix = 0
        if (top_len % 2 == 0) and (len(custom_header) % 2 == 0):
            fix = 0
        else:
            fix = -1
        pushers_right = ' ' * (((top_len // 2) - (len(custom_header) // 2)) + fix + manual_fix)

        header = '|| {0}{1}{2} ||'.format(pushers_left, custom_header, pushers_right)
    else:
        msg('ERROR: unexpected header_type ({0})'.format(header_type), 1)
    return header


# `mb
def msgbox(text='Example', header_type=0, custom_header='CUSTOM', manual_fix=0):
    # header_type possible values:
    # 0 = INFO
    # 1 = ERROR
    # 2 = WARNING

    # easy support for '\n' (real new-lines)
    if text.__contains__('\n'):
        text = text.replace('\n', '\\n')

    # for text with '\\n' (strings representing new lines)
    if text.__contains__('\\n'):
        lines = text.split('\\n')  # create list with lines from the text

        # find the longest line's length (needed to create ceiling & floor of the TextMessageBox)
        top_len = 0
        for line in lines:
            if len(line) > top_len:
                top_len = len(line)
        if header_type == 0:
            if top_len < 4:
                top_len = 4
        if header_type == 1:
            if top_len < 5:
                top_len = 5
        if header_type == 2:
            if top_len < 7:
                top_len = 7
        if header_type == 3:
            if len(custom_header) > top_len:
                top_len = len(custom_header)

        print('=' * (top_len + 6))  # print the ceiling
        pushers = ' ' * top_len
        print('|| {0} ||'.format(pushers))  # print only pipes & spaces to make it prettier
        print(make_msgbox_header(top_len, header_type, custom_header, manual_fix))

        # print the text line by line
        for line in lines:
            pushers = (' ' * (top_len - len(line)))  # spaces which push the ' ||' to the right edge
            print('|| {0}{1} ||'.format(line, pushers))

        pushers = ' ' * top_len
        print('|| {0} ||'.format(pushers))  # print only pipes & spaces to make it prettier
        print('=' * (top_len + 6))  # print the floor

    # simple string without new-lines
    else:
        # find the longest line's length (needed to create ceiling & floor of the TextMessageBox)
        top_len = len(text)
        if header_type == 0:
            if top_len < 4:
                top_len = 4
        if header_type == 1:
            if top_len < 5:
                top_len = 5
        if header_type == 2:
            if top_len < 7:
                top_len = 7
        if header_type == 3:
            if len(custom_header) > top_len:
                top_len = len(custom_header)

        pushers = ' ' * top_len

        # adds 6 equals-characters so the '|| ' & ' ||' will fit between the equals-chars
        print('=' * (top_len + 6))

        print('|| {0} ||'.format(pushers))  # print only pipes & spaces to make it prettier
        print(make_msgbox_header(top_len, header_type, custom_header, manual_fix))
        text_pushers = ' ' * (len(pushers) - len(text))
        print('|| {0}{1} ||'.format(text, text_pushers))
        print('|| {0} ||'.format(pushers))  # print only pipes & spaces to make it prettier
        print('=' * (top_len + 6))

    # wait for user to input anything (or just press ENTER) so the msgbox won't be cleared before showing the menu
    if Config.clear_before_menu and not launched_gfx_mode:
        inputx()

# `msg `message
def msg(text='Example', header_type=0, custom_header='CUSTOM', manual_fix=0, terminal_only=False, use_pprint=False):
    if launched_gfx_mode and not terminal_only and not args.no_pyqt5:
        header = Text.info
        if header_type == 1:
            header = Text.error
            QtWidgets.QMessageBox.critical(None, header, text)
        elif header_type == 2:
            header = Text.warning
            QtWidgets.QMessageBox.warning(None, header, text)
        elif header_type == 3:
            header = custom_header
            QtWidgets.QMessageBox.information(None, header, text)
        else:
            QtWidgets.QMessageBox.information(None, header, text)
    elif Config.use_msgbox:
        msgbox(text, header_type, custom_header, manual_fix)
    else:
        header = '+-+-INFO-+-+:'
        use_header = True
        
        if header_type == 1:
            header = '+-+-ERROR-+-+:'
        elif header_type == 2:
            header = '+-+-WARNING-+-+:'
        elif header_type == 3:
            header = custom_header
        elif header_type == 4:
            use_header = False

        print()
        if use_header:
            print('{h}\n'.format(h=header))
        if use_pprint:
            pprint(text, width=1)
        else:
            print(text)
        print()
        if Config.clear_before_menu and not launched_gfx_mode:
            inputx()


# Extended version of input() function which interprets NetBar commands and supports redo
def inputx(prompt=''):
    global current_input, previous_input

    current_input = input(prompt)
    
    if current_input == 'help':
        print(f'Use {Config.cmd_char}help')
        current_input = inputx(prompt)
    # inputx commands
    if re.search('^{}'.format(Config.cmd_char), current_input):
        c = re.sub(Config.cmd_char, '', current_input)

        if c == 'r' or c == 'redo':
            current_input = previous_input
        elif c == 'prev_value':
            print('\n{0}\n'.format(previous_input))
            current_input = inputx()

    while command_interpreter(current_input) == 0:
        previous_input = current_input
        current_input = inputx(prompt)
    previous_input = current_input
    return current_input


def _3xplus1(n):
    if n == 1:  # the end
        return n
    elif (n % 2) == 0:  # even
        n = n / 2
        print(n)
        _3xplus1(n)
    else:  # odd
        n = 3 * n + 1
        print(n)
        _3xplus1(n)

def open_url_from_file(filepath):
    f = open(filepath)
    _url = f.readline().rstrip('\n')
    init_browser(_url, exit_when_done=True)

def add_to_history(entry=''):
    try:
        if entry not in history:
            if entry:
                history.append(entry)
            hjson.dump(history, open(HISTORY_FILE, 'w'), indent='\t')
    except IOError as exc:
        print('add_to_history():')
        print(exc)
        print()
        traceback.print_tb(exc.__traceback__)

def load_history():
    global history
    if os.path.exists(HISTORY_FILE):
        data = hjson.load(open(HISTORY_FILE))
        if isinstance(data, list):
            if len(data) < len(history):
                msg('Loaded history is smaller than history in memory, creating history backup...', 2, terminal_only=True)
                backup_history()
            history = data
        else:
            msg('Loaded history file is corrupted, creating history backup...', terminal_only=True)
            backup_history()
    else:
        msg(f'{HISTORY_FILE} not found! Attempting to restore backup...', 1, terminal_only=True)
        if restore_history():
            load_history()
        else:
            add_to_history()
            msg('Restore was unsuccessful, fresh history file created.', terminal_only=True)


""" Dumps history from system memory which is known to be not corrupted to separate file which can be later restored"""
def backup_history():
    data = list(history)
    with open(f"{HISTORY_FILE}.backup", 'w') as fh:
        hjson.dump(data, fh, indent='\t')
    msg('History backup has been created!', terminal_only=True)

def restore_history():
    his_bkp = f"{HISTORY_FILE}.backup"
    if os.path.isfile(his_bkp):
        os.remove(HISTORY_FILE)
        os.rename(his_bkp, HISTORY_FILE)
        msg('History has been restored from backup!', terminal_only=True)
        return True
    else:
        msg(f'{his_bkp} not found!', 1)
        return False

def launch_gfx(firstTime=False):
    if launched_gfx_mode and not firstTime:
        msg('Graphical mode is already active!', 2, terminal_only=True)
    elif args.no_pyqt5:
        msg('PyQt5 is disabled via --no-pyqt5 option.', terminal_only=True)
    elif not pyqt5_available:
        msg('Unable to use graphical mode: PyQt5 framework not found.', 1, terminal_only=True)
    else:
        graphical_mode()

def exc_func(exc):
    try:
        msg(str(exc), 1)
        traceback.print_tb(exc.__traceback__)
    except Exception as excTwo:
        print(excTwo)

    try:
        # log error
        with open('errors.log', 'a') as fh:
            current_time = time.strftime('%Y.%m.%d %H:%M:%S', time.localtime(time.time()))
            print(f"{current_time}:", file=fh)
            print(f"Platform: {sys.platform}", file=fh)
            print(exc, file=fh)
            traceback.print_tb(exc.__traceback__, file=fh)
            print('\n', file=fh)
    except Exception as exc2:
        print(f"Error while handling previous error: {exc2}")
        traceback.print_tb(exc.__traceback__)

exch = exch_factory(exc_block=exc_func)
exch_with_args = exch_factory(exc_block=exc_func, pass_args=True)

def graphical_mode():
    class QuickAddWindow(QtWidgets.QDialog):
        @exch
        def __init__(self):
            super().__init__()
            self.setWindowTitle(Text.quickadd)

            menubar = QtWidgets.QMenuBar(self)
            closeAction = QtWidgets.QAction('', self)
            closeAction.setShortcut('Ctrl+W')
            closeAction.triggered.connect(self.close)
            menubar.addAction(closeAction)

            nameLabel = QtWidgets.QLabel(f'{Text.name}:')
            self.name = QtWidgets.QLineEdit()
            nameHBox = QtWidgets.QHBoxLayout()
            nameHBox.addWidget(nameLabel)
            nameHBox.addWidget(self.name)

            keyLabel = QtWidgets.QLabel(f'{Text.key}:')
            self.key = QtWidgets.QLineEdit()
            keyHBox = QtWidgets.QHBoxLayout()
            keyHBox.addWidget(keyLabel)
            keyHBox.addWidget(self.key)

            urlLabel = QtWidgets.QLabel(f'{Text.url}:')
            self.URL = QtWidgets.QLineEdit()
            urlHBox = QtWidgets.QHBoxLayout()
            urlHBox.addWidget(urlLabel)
            urlHBox.addWidget(self.URL)

            addBtn = QtWidgets.QPushButton(Text.add)
            addBtn.clicked.connect(self.quickAdd)

            mainVerticalBox = QtWidgets.QVBoxLayout()

            mainVerticalBox.addLayout(nameHBox)
            mainVerticalBox.addLayout(keyHBox)
            mainVerticalBox.addLayout(urlHBox)
            mainVerticalBox.addWidget(addBtn)
            mainVerticalBox.setAlignment(addBtn, Qt.AlignRight)

            self.setLayout(mainVerticalBox)

            msg(Text.quickadd_manual)
        
        @exch
        def quickAdd(self):
            url = self.URL.text()
            label = self.name.text()
            key = self.key.text()
            if not url:
                url = app.clipboard().text()
                msg(f'{Text.url_from_clipboard}:\n{url}')
            quick_add(url, label, key)

    class MainWindow(QtWidgets.QWidget):
        @exch
        def __init__(self):
            super(MainWindow, self).__init__()

            self.setWindowIcon(QtGui.QIcon(os.path.join('img', 'icon.png')))
            self.setGeometry(300, 300, 500, 100)
            self.setWindowTitle('NetBar {}'.format(VERSION))
            self.setObjectName('mainWindow')
            self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)
            subVBox = QtWidgets.QVBoxLayout()

            menuBar = QtWidgets.QMenuBar()
            if Config.gui_pl100:
                menuBar.setStyleSheet('.QMenuBar{background-color: white;}')
            else:
                menuBar.setStyleSheet('.QMenuBar{background-color: #1e1e1e; color: white;}')
            menuBar.setMaximumHeight(30)

            if Config.gui_bg_colour is not None:
                self.setStyleSheet(
                    """
                    #mainWindow{background-color: %s;}
                    """ % (Config.gui_bg_colour)
                )
                menuBar.setStyleSheet(
                    """
                    QMenuBar{background-color: %s; color: white;}
                    """ % (Config.gui_bg_colour)
                )

            # Main
            mainMenu = menuBar.addMenu(f'&{Text.main}')
            searchOrVisitAction = QtWidgets.QAction(f'&{Text.search_or_visit}', self)
            searchOrVisitAction.setShortcut('Return')
            searchOrVisitAction.triggered.connect(lambda: self.searchOrVisit())
            mainMenu.addAction(searchOrVisitAction)

            forceSearchOrVisitAction = QtWidgets.QAction(f"{Text.search_or_visit} ({Text.force})", self)
            forceSearchOrVisitAction.setShortcut('Ctrl+Return')
            forceSearchOrVisitAction.triggered.connect(lambda: self.searchOrVisit(allow_repeated_search=True))
            mainMenu.addAction(forceSearchOrVisitAction)

            forceSearchAction = QtWidgets.QAction(Text.force_search, self)
            forceSearchAction.setShortcut('Alt+Return')
            forceSearchAction.triggered.connect(self.forceSearch)
            mainMenu.addAction(forceSearchAction)

            openConfigAction = QtWidgets.QAction(Text.open_config, self)
            openConfigAction.setShortcut('Ctrl+Shift+C')
            openConfigAction.triggered.connect(open_config)
            mainMenu.addAction(openConfigAction)

            if Config.gui_pl100:
                POEM = """
Mateusz Dargacz - "Sto lat Polsko!"

Nasi rodacy już nie raz wygrali,
nie było lekko lecz się nie poddali.
Przetrwaliśmy Stalina, przetrwaliśmy Hitlera,
już nami nikt nie sponiewiera!
Dziś świętujemy 100 lat niepodległości,
niechaj w naszych sercach duma i radość zagości!
"""

                poemAction = QtWidgets.QAction('Wiersz', self)
                poemAction.triggered.connect(lambda: msg(POEM))
                mainMenu.addAction(poemAction)
            
            updateAction = QtWidgets.QAction(Text.update, self)
            updateAction.triggered.connect(self.updateNetBar)
            mainMenu.addAction(updateAction)
        

            restartAction = QtWidgets.QAction(Text.restart, self)
            restartAction.setShortcut('Ctrl+R')
            restartAction.triggered.connect(restart)
            mainMenu.addAction(restartAction)

            quitAction = QtWidgets.QAction(f'&{Text.quit}', self)
            quitAction.setShortcut('Ctrl+Q')
            quitAction.triggered.connect(lambda: sys.exit())
            mainMenu.addAction(quitAction)

            # Edit
            editMenu = menuBar.addMenu(f'&{Text.edit}')
            clearAction = QtWidgets.QAction(f'&{Text.clear_text}', self)
            clearAction.setShortcut('Alt+Delete')
            clearAction.triggered.connect(self.clearText)
            editMenu.addAction(clearAction)
            
            clearAndPasteAction = QtWidgets.QAction(f'{Text.clear_and_paste}', self)
            clearAndPasteAction.setShortcut('Ctrl+Shift+V')
            clearAndPasteAction.triggered.connect(self.clearAndPaste)
            editMenu.addAction(clearAndPasteAction)

            showStylesAction = QtWidgets.QAction(Text.show_available_styles, self)
            showStylesAction.triggered.connect(self.showStyles)
            editMenu.addAction(showStylesAction)

            showSEkeymapAction = QtWidgets.QAction(Text.show_search_engines_mapping, self)
            showSEkeymapAction.triggered.connect(self.showSEkeymap)
            showSEkeymapAction.setShortcut('Ctrl+M')
            editMenu.addAction(showSEkeymapAction)

            showSEkeymapReversedAction = QtWidgets.QAction(Text.show_search_engines_mapping, self)
            showSEkeymapReversedAction.triggered.connect(lambda: self.showSEkeymap(reverse=True))
            showSEkeymapReversedAction.setShortcut('Ctrl+Shift+M')
            editMenu.addAction(showSEkeymapReversedAction)

            focusOnTextboxAction = QtWidgets.QAction(Text.focus_on_textbox, self)
            focusOnTextboxAction.setShortcut('F6')
            focusOnTextboxAction.triggered.connect(self.focusOnTextbox)
            editMenu.addAction(focusOnTextboxAction)

            quickAddAction = QtWidgets.QAction(f'&{Text.quickadd}', self)
            quickAddAction.setShortcut('Ctrl+N')
            quickAddAction.triggered.connect(self.showQuickAddDialog)
            editMenu.addAction(quickAddAction)

            saveWidthAction = QtWidgets.QAction(Text.save_win_width, self)
            saveWidthAction.triggered.connect(self.saveWidth)
            editMenu.addAction(saveWidthAction)
            
            savePos = QtWidgets.QAction(Text.save_win_pos, self)
            savePos.triggered.connect(self.savePosition)
            editMenu.addAction(savePos)

            # <dev>
            if dev:
                devMenu = menuBar.addMenu('&Dev')
                
                testAction = QtWidgets.QAction('&Test', self)
                testAction.setShortcut('Ctrl+T')
                testAction.triggered.connect(self.test)
                devMenu.addAction(testAction)

                mdqxmodAction = QtWidgets.QAction('&mdqxmod version', self)
                mdqxmodAction.triggered.connect(self.check_mdqxmod_ver)
                devMenu.addAction(mdqxmodAction)

                snpAction = QtWidgets.QAction('&show SNP', self)
                snpAction.setShortcut('Ctrl+Shift+P')
                snpAction.triggered.connect(self.show_snp)
                devMenu.addAction(snpAction)
            # </dev>

            if Config.gui_pl100:
                # PL.100
                pl100Action = QtWidgets.QAction('PL 100?', self)
                pl100Action.triggered.connect(lambda: init_browser('https://pl.wikipedia.org/wiki/Obchody_100-lecia_odzyskania_niepodleg%C5%82o%C5%9Bci_przez_Polsk%C4%99'))
                menuBar.addAction(pl100Action)

            settingsAction = QtWidgets.QAction(Text.sett_settings, self)
            settingsAction.setShortcut('Ctrl+Shift+S')
            settingsAction.triggered.connect(self.openSettings)
            menuBar.addAction(settingsAction)

            # HotKeys
            # using separate menubar to hide it
            hotkeysMenuBar = QtWidgets.QMenuBar()
            hotkeysMenu = hotkeysMenuBar.addMenu('')
            # hotkeysMenu.resize(0, 0)
            csf0_Action = QtWidgets.QAction('CSF 0', self)
            csf0_Action.setShortcut('Ctrl+0')
            csf0_Action.triggered.connect(lambda: self.selectCSF(0))
            hotkeysMenu.addAction(csf0_Action)

            csf1_Action = QtWidgets.QAction('CSF 1', self)
            csf1_Action.setShortcut('Ctrl+1')
            csf1_Action.triggered.connect(lambda: self.selectCSF(1))
            hotkeysMenu.addAction(csf1_Action)

            csf2_Action = QtWidgets.QAction('CSF 2', self)
            csf2_Action.setShortcut('Ctrl+2')
            csf2_Action.triggered.connect(lambda: self.selectCSF(2))
            hotkeysMenu.addAction(csf2_Action)

            csf3_Action = QtWidgets.QAction('CSF 3', self)
            csf3_Action.setShortcut('Ctrl+3')
            csf3_Action.triggered.connect(lambda: self.selectCSF(3))
            hotkeysMenu.addAction(csf3_Action)

            csf4_Action = QtWidgets.QAction('CSF 4', self)
            csf4_Action.setShortcut('Ctrl+4')
            csf4_Action.triggered.connect(lambda: self.selectCSF(4))
            hotkeysMenu.addAction(csf4_Action)

            csf5_Action = QtWidgets.QAction('CSF 5', self)
            csf5_Action.setShortcut('Ctrl+5')
            csf5_Action.triggered.connect(lambda: self.selectCSF(5))
            hotkeysMenu.addAction(csf5_Action)

            csf6_Action = QtWidgets.QAction('CSF 6', self)
            csf6_Action.setShortcut('Ctrl+6')
            csf6_Action.triggered.connect(lambda: self.selectCSF(6))
            hotkeysMenu.addAction(csf6_Action)

            csf7_Action = QtWidgets.QAction('CSF 7', self)
            csf7_Action.setShortcut('Ctrl+7')
            csf7_Action.triggered.connect(lambda: self.selectCSF(7))
            hotkeysMenu.addAction(csf7_Action)

            csf8_Action = QtWidgets.QAction('CSF 8', self)
            csf8_Action.setShortcut('Ctrl+8')
            csf8_Action.triggered.connect(lambda: self.selectCSF(8))
            hotkeysMenu.addAction(csf8_Action)

            csf9_Action = QtWidgets.QAction('CSF 9', self)
            csf9_Action.setShortcut('Ctrl+9')
            csf9_Action.triggered.connect(lambda: self.selectCSF(9))
            hotkeysMenu.addAction(csf9_Action)

            for char in ascii_lowercase:
                hotkeyAction = QtWidgets.QAction('', self)
                hotkeyAction.setShortcut(f"Ctrl+Alt+{char}")
                hotkeyAction.triggered.connect(lambda _, c=char: self.selectCSF(ord(c)))
                hotkeysMenuBar.addAction(hotkeyAction)

            # loading animation widget
            self.loadingBtn = QtWidgets.QPushButton()
            self.loadingBtn.setMaximumSize(31, 31)
            self.loadingBtn.setStyleSheet("QPushButton{background-color: %s}" % (Config.gui_bg_colour))
            self.loadingBtn.setVisible(False)

            hotkeysMenuBar.setMaximumSize(0, 0)
            sub2HBox = QtWidgets.QHBoxLayout()
            sub2HBox.addWidget(hotkeysMenuBar)
            sub2HBox.addWidget(menuBar)
            sub2HBox.addWidget(self.loadingBtn)
            subVBox.addLayout(sub2HBox)

            self.textBox = QtWidgets.QLineEdit()
            self.textBox.setMaximumHeight(30)
            if Config.use_suggestions:
                self.updateSuggestions()
            # PL.100
            if Config.gui_pl100:
                self.textBox.setStyleSheet('.QLineEdit{background-color: red; color: white;border: 0;}')
            else:
                self.textBox.setStyleSheet('.QLineEdit{background-color: white; color: black;border: 0;}')

            self.csfComboBox = QtWidgets.QComboBox()
            # labels = list(csf_label_filepath.keys())
            labels = list(Config.se_keymap.dict.keys())
            labels.sort()
            # To make default search engine selected in ComboBox
            for csf_label in labels:
                self.csfComboBox.addItem(csf_label)
            self.selectCSF(Config.def_se)
            self.csfComboBox.setFont(QtGui.QFont('Verdana', Config.gui_csf_box_font_size))

            mainHBox = QtWidgets.QHBoxLayout()
            subVBox.addWidget(self.textBox)
            mainHBox.addLayout(subVBox)
            mainHBox.addWidget(self.csfComboBox)
            subVBox.setContentsMargins(QtCore.QMargins(0, 0, 0, 0))
            mainHBox.setSpacing(0)
            self.setLayout(mainHBox)
            self.setStyleSheet('#mainWindow{background-color: #1e1e1e;}')

            # PL.100
            if Config.gui_pl100:
                self.csfComboBox.setStyleSheet('QComboBox::editable{background-color: #1e1e1e;color: white;}QComboBox::!editable{selection-background-color: red;}')
            else:
                self.csfComboBox.setStyleSheet('QComboBox::editable{background-color: #1e1e1e;color: white;}QComboBox::!editable{selection-background-color: #1e1e1e;}')
            self.setMinimumWidth(283)
            self.setMinimumHeight(83)
            self.setMaximumHeight(83)
            self.resize(270, 83)
            self.textBox.setFont(QtGui.QFont('Verdana', Config.gui_text_box_font_size))

            # for element in dir(hotkeysMenu): # !TEST!
            # 	 if re.fullmatch(r'set.*Name.*', element):
            # 		 print(element)

            if Config.gui_width:
                self.resize(Config.gui_width, self.height())
            if Config.gui_position:
                self.move(*Config.gui_position)

            self.white_bg = True
            self.show()
            self.textBox.setFocus()
        
        @exch_with_args
        def selectCSF(self, index_or_label):
            try:
                if isinstance(index_or_label, int):
                    label = csf_key_label[index_or_label]
                elif isinstance(index_or_label, str):
                    label = index_or_label
                else:
                    raise TypeError(f'{Text.wrong_index_label_type}: {type(index_or_label)}')

                for index in range(self.csfComboBox.count()):
                    if self.csfComboBox.itemText(index) == label:
                        self.csfComboBox.setCurrentIndex(index)
            except KeyError:
                msg(f'{Text.no_csf_mapped_to_key}: {index_or_label}', 1)

        @exch
        def updateSuggestions(self):
            load_history()
            completer = QtWidgets.QCompleter(history)
            completer.setCaseSensitivity(False)
            completer.setCompletionMode(completer.InlineCompletion)
            self.textBox.setCompleter(completer)

        @exch
        def updateNetBar(self):
            if Config.use_updater:
                try_update(VERSION, pyqt5_ready=True)
            else:
                msg(Text.updater_n_a, 1)

        @exch_with_args
        def searchOrVisit(self, allow_repeated_search=False):
            global previous_input
            text = self.textBox.text()
            if Config.block_repeated_search and not allow_repeated_search:
                if previous_input == text:
                    msg(Text.blocked_repeated_search)
                    return
                else:
                    previous_input = text
            add_to_history(text)
            if Config.use_suggestions:
                self.updateSuggestions()
                
            if command_interpreter(text) != 0:
                if is_uri(text):
                    init_browser(text, True)
                else:
                    csf = load_csf(self.csfComboBox.currentText())
                    search(text, csf, True)

            # STOS™ - Switch Theme On Search
            if Config.use_STOS:
                if self.white_bg:
                    self.textBox.setStyleSheet('.QLineEdit{background-color: black; color: white;border: 0;}')
                    self.white_bg = False
                else:
                    self.textBox.setStyleSheet('.QLineEdit{background-color: white; color: black;border: 0;}')
                    self.white_bg = True
            
            # loading animation init
            if Config.use_loading_anim:
                threading.Thread(target=self.rotateLoadingImg).start()

        @exch
        def forceSearch(self):
            csf = load_csf(self.csfComboBox.currentText(), use_regex=False)
            text = self.textBox.text()
            search(text, csf, exit_when_done=False)

        @exch
        def clearText(self):
            self.textBox.clear()
        
        @exch
        def clearAndPaste(self):
            self.clearText()
            self.textBox.setText(app.clipboard().text())
        
        @exch
        def showStyles(self):
            styles = ('_' * 30) + '\n'
            for style in QtWidgets.QStyleFactory.keys():
                styles += '• {}\n'.format(style)
            msg(styles, 3, Text.available_styles)

        @exch_with_args
        def showSEkeymap(self, reverse=False):
            keymap = ('_' * 30) + '\n'
            if reverse:
                for key in sorted(csf_key_label.keys()):
                    label = csf_key_label[key]
                    actual_key = key
                    if actual_key > 9:
                        actual_key = chr(actual_key).upper()
                    keymap += f"{actual_key}: {label}\n"
            else:
                for key, label in csf_key_label.items():
                    actual_key = key
                    if actual_key > 9:
                        actual_key = f"Ctrl + Alt + {chr(actual_key).upper()}"
                    else:
                        actual_key = f"Ctrl + {actual_key}"
                    keymap += f"{label}: {actual_key}\n"
            msg(keymap, header_type=3, custom_header=Text.se_keymap)

        @exch
        def focusOnTextbox(self):
            self.textBox.setFocus(True)

        @exch
        def showQuickAddDialog(self):
            dialog = QuickAddWindow()
            dialog.exec()

        @exch
        def saveWidth(self):
            Config.gui_width = self.width()
        
        @exch
        def savePosition(self):
            Config.gui_position = (self.x(), self.y())
        
        # <dev>
        @exch
        def test(self):
            self.loadingDialog = QtWidgets.QDialog(self)
            self.loadingDialog.resize(300, 300)
            self.loadingDialog.setWindowTitle('Loading test')
            layout = QtWidgets.QVBoxLayout()

            loadingDialogMenuBar = QtWidgets.QMenuBar()
            loadingDialogMenuBar.setMaximumSize(0, 0)
            closeAction = QtWidgets.QAction('', self)
            closeAction.setShortcut('Ctrl+W')
            closeAction.triggered.connect(self.loadingDialog.close)
            loadingDialogMenuBar.addAction(closeAction)
            layout.addWidget(loadingDialogMenuBar)

            self.loadingBtn = QtWidgets.QPushButton()
            self.loadingBtn.setMaximumSize(31, 31)
            layout.addWidget(self.loadingBtn)
            self.loadingDialog.setLayout(layout)
            self.loadingActive = True
            threading.Thread(target=self.rotateLoadingImg).start()
            self.loadingDialog.exec()
            self.loadingActive = False
        
        @exch
        def rotateLoadingImg(self):
            loadingIconFileName = 'loading1.png'
            loadingTimeElapsed = 0.00
            self.loadingBtn.setVisible(True)
            while 1:
                index = re.search(r'(\d)', loadingIconFileName).group(1)
                index = int(index)
                index += 1
                if index > 11:
                    index = 1
                loadingIconFileName = f"loading{index}.png"
                loadingIcon = QtGui.QIcon(os.path.join('img', loadingIconFileName))
                self.loadingBtn.setIcon(loadingIcon)
                self.loadingBtn.setIconSize(QtCore.QSize(30, 30))
                time.sleep(.08)
                loadingTimeElapsed += .08
                if dev:
                    print(f'loading time elapsed = {loadingTimeElapsed}')
                if loadingTimeElapsed > 1.5:
                    self.loadingBtn.setVisible(False)
                    break
        
        @exch
        def check_mdqxmod_ver(self):
            from mdqxmod import __version__ as mver
            msg(f'mdqxmod {mver}')
        
        # snp = size 'n' position
        @exch
        def show_snp(self):
            msg(f"size: {self.width()} x {self.height()}\npos: ({self.x()}, {self.y()})")
        # </dev>

        @exch
        def openSettings(self):
            import settings
            self.settingsWindow = settings.MainWindow()
            self.settingsWindow.exec()
            if self.settingsWindow.tabWidget.required_action == settings.ACTION_RESTART:
                restart()
            else:
                reload()
            

        # Hotkeys
        @exch
        def select_csf0(self):
            self.selectCSF(0)

    global app
    app = QtWidgets.QApplication(sys.argv)
    if Config.gui_style is not None:
        if Config.gui_style in QtWidgets.QStyleFactory.keys():
            style = QtWidgets.QStyleFactory.create(Config.gui_style)
            app.setStyle(style)
        else:
            msg('{} style not found. Using default.'.format(Config.gui_style), 2)

    global win
    win = MainWindow()
    sys.exit(app.exec())

# +-+-MAIN-+-+
if __name__ == '__main__':
    try:

        
        if not os.path.isdir('csf'):
            os.mkdir('csf')
        
        # set CMD window title if running on Windows NT
        if sys.platform == 'win32':
            os.system('title NetBar {0} (CLI mode)'.format(VERSION))

        reload()

        if os.path.isfile('updater.py'):
            from updater import try_update
            Config.use_updater = True
        else:
            Config.use_updater = False

        # checks whether another instance (i.e window) is already opened and if so switches focus to it
        if sys.platform == 'win32' and Config.try_focus_prev_instance:
            try:
                from pywinauto.application import Application
                from pywinauto import ElementNotFoundError
                prog = Application().connect(backend='win32', title=f'NetBar {VERSION}', visible_only=True)
                if prog is not None:
                    prog.window().set_focus()
                    sys.exit()
            except ElementNotFoundError as ex:
                pass

        if Config.browser is None:
            detect_browser()
        if Config.file_mgr is None:
            Config.file_mgr = 'caja' if sys.platform == 'linux' else 'explorer'
        if Config.text_editor is None:
            Config.text_editor = 'pluma' if sys.platform == 'linux' else 'notepad.exe'

        if Config.check_for_updates and Config.use_updater:
            try_update(VERSION)

        if args.gfx:
            pass
        if args.cli:
            launched_gfx_mode = False
        if isinstance(args.command, str):
            command_interpreter(args.command, escape=True)
        if isinstance(args.url, str):
            init_browser(args.url)
        if isinstance(args.search, str):
            search_or_visit(args.search, exit_when_done=True)
        if isinstance(args.search_with, (tuple, list)):
            text, CSF = args.search_with
            csf = load_csf(CSF, use_regex=True)
            search(text, csf, exit_when_done=True)
        # Support for launching browser with URL saved in a text-file
        if args.from_file:
            open_url_from_file(args.from_file)
        if args.mkcsf:
            make_csf()
        if args.conv_csf:
            convert_csf()
        if args.quit:
            sys.exit()
        
        if not args.cli:
            launched_gfx_mode = True

        if launched_gfx_mode:
            launch_gfx(True)
        else:
            menu()
    except Exception as exc:
        exc_func(exc)